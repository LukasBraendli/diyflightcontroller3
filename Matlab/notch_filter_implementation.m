%clear all;
%close all;
theta0=.3*2*pi;
rho=.9; % Selectable: theta0=notch frequency, rho=controls BW 
b=[1 -2*rho*cos(theta0) rho^2];
a=b(end:-1:1);
num=(b+a)/2;
den=a;
fvtool(num,den);
title('Notch Filter response')