/*
 * interrupts.c
 *
 *  Created on: 12.04.2020
 *      Author: IchWerSonst
 */

#include "interrupt_callbacks.h"

#include "icm20602.h"
#include "sbus.h"


/**
 * @brief GPIO external interrupt callback
 * gets called by interrupt request handler
 */
void HAL_GPIO_EXTI_Callback(uint16_t GPIO_Pin)
{
	icm20602_EXTIHandler(GPIO_Pin);	// call icm20602 interrupt handler
}

/**
 * @brief uart receive complete callback
 * gets called by UART_DMAReceiveCplt(...) in stm32f7xx_hal_uart.c
 */
void HAL_UART_RxCpltCallback(UART_HandleTypeDef *huart)
{
	sbus_uartRxCplt(huart, &receiver);
}
