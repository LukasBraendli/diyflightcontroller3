/*
 * bat_measure.c
 *
 *  Created on: 07.06.2020
 *      Author: IchWerSonst
 */


#include "bat_measure.h"


/**
 * @brief initialize battery measuring system
 * - initializes adc peripheral
 */
void bat_measure_init(void)
{
	/* ADC clock enable */
	RCC->APB2ENR |= RCC_APB2ENR_ADC1EN;
	//__HAL_RCC_ADC1_CLK_ENABLE();

	ADC->CCR |= 0x01 << ADC_CCR_ADCPRE_Pos;	// set ADC prescaler to 4

	ADC1->CR1 |= ADC_CR1_EOCIE;					// enable end of conversion interrupt
	ADC1->SMPR1 |= ADC_NUM_SAMPLES << ADC_SMPR1_SMP10_Pos;	// set sampling cycles for channel 10
	ADC1->SQR1 |= 0x00 << ADC_SQR1_L_Pos;		// set sequence length to 0 (= 1 conversion)
	ADC1->SQR3 |= 0x0A << ADC_SQR3_SQ1_Pos;		// set 1st conversion to 10 (= channel 10)
	ADC1->CR2 |= ADC_CR2_ADON;					// enable ADC

	// enable ADC global interrupts
	HAL_NVIC_SetPriority(ADC_IRQn, 0, 0);
	HAL_NVIC_EnableIRQ(ADC_IRQn);

	num_current_bat_meas_samples = 0;
	bat_meas_ADCAvg = 0;
	bat_meas_batVoltage = 0;
	bat_meas_busyFlag = 0;

	// find out how many cells the connected battery has
	// for that average over 10 measurements and guess based on that
	HAL_Delay(10);	//let voltage stabilize
	float v_avg = 0;
	for(int i = 0; i < 10; i++)
	{
		bat_measure_start();
		// wait for measurement to complete
		while(bat_meas_busyFlag)
		{
			asm("nop");	// do nothing
		}
		v_avg += bat_measure_getBatVoltage();
	}

	v_avg /= 10;

	if(v_avg > LIPO_3S_VMIN && v_avg < LIPO_3S_VMAX)
	{
		lipo_num_cells = LIPO_3S;
		lipo_vmax = LIPO_3S_VMAX;
		lipo_vcrit = LIPO_3S_VCRIT;
		lipo_vmin = LIPO_3S_VMIN;
	}
	else if(v_avg > LIPO_4S_VMIN && v_avg < LIPO_4S_VMAX + 0.5)
	{
		lipo_num_cells = LIPO_4S;
		lipo_vmax = LIPO_4S_VMAX;
		lipo_vcrit = LIPO_4S_VCRIT;
		lipo_vmin = LIPO_4S_VMIN;
	}
	else
	{
		lipo_num_cells = LIPO_CELL_ERR;
		lipo_vmax = LIPO_CELL_ERR;
		lipo_vcrit = LIPO_CELL_ERR;
		lipo_vmin = LIPO_CELL_ERR;
	}


	// Compute PID scaling factor according to battery voltage; 3S := 100%
	if(lipo_num_cells == LIPO_3S)
	{
		lipo_pid_scalar = PID_3S_SCALAR;
	}
	else if(lipo_num_cells == LIPO_4S)
	{
		lipo_pid_scalar = PID_4S_SCALAR;
	}
	else
	{
		lipo_pid_scalar = 1;
	}
}


/**
 * @brief start battery measurement
 * -->	this starts a series of NUM_BAT_MEAS_SAMPLES measurements
 * 		to get an average voltage reading
 */
void bat_measure_start(void)
{
	if(!bat_meas_busyFlag)
	{
		bat_meas_busyFlag = 1;
		ADC1->CR2 |= ADC_CR2_SWSTART;	// start ADC conversion
	}
}


/**
 * @brief adc conversion complete interrupt handler
 * starts a new conversion as long as less than NUM_BAT_MEAS_SAMPLES are done yet
 */
void bat_measure_EOCHandler(void)
{
	if(num_current_bat_meas_samples >= NUM_BAT_MEAS_SAMPLES - 1)
	{
		num_current_bat_meas_samples++;
		bat_meas_ADCAvg += (double)ADC1->DR;

		bat_meas_ADCAvg /= num_current_bat_meas_samples;
		bat_meas_batVoltage = (float)(3.3 * bat_meas_ADCAvg / 4096 * VOLTAGE_DIV_RATIO);

		num_current_bat_meas_samples = 0;
		bat_meas_ADCAvg = 0;
		bat_meas_busyFlag = 0;
	}
	else
	{
		num_current_bat_meas_samples++;
		bat_meas_ADCAvg += (double)ADC1->DR;

		ADC1->CR2 |= ADC_CR2_SWSTART;	// start next ADC conversion
	}
}


/**
 * @brief returns the last measured average battery voltage
 * @retval the battery voltage in volts
 */
float bat_measure_getBatVoltage(void)
{
	return bat_meas_batVoltage;
}


/**
 * @brief returns the lipo_pid_scalar variable
 * @retval lipo_pid_scalar
 */
float bat_measure_getPIDScalar(void)
{
	return lipo_pid_scalar;
}
