/*
 * timers_custom.c
 *
 *  Created on: 18.04.2020
 *      Author: IchWerSonst
 */

#include "timers_custom.h"
#include "stm32f722xx.h"
#include "dshot.h"
#include "buzzer.h"


/* TIM2 init function
 * set TIM2 up as a micro seconds ticker
 * NOTE: as TIM2 is a 32bit timer, it will run for ~70 minutes before overflow occurs!
 * !---WARNING---! If the program is needed to run longer than 70 minutes it is advised to implement a solution to the overflow problem.
 * */
void TIM2_Init(void)
{
	RCC->APB1ENR |= RCC_APB1ENR_TIM2EN;	// enable timer 2 clock

	//TIM2->CR1 &= ~(0b0000101111111111);
	TIM2->CNT = (uint32_t)0x00000000;	// reset counter value
	TIM2->PSC = (uint16_t)((HAL_RCC_GetPCLK1Freq() * 2 / 1000000) - 1);	// set prescaler
	TIM2->EGR |= TIM_EGR_UG;	// manually generate update event to load the previously set, buffered prescale value
	TIM2->ARR = (uint32_t)(0xFFFFFFFF - 1);	// set timer period (timer counts up to this number, then resets to 0 in up-counting mode)

	TIM2->CR1 |= TIM_CR1_CEN;	// enable counter
}

uint32_t microseconds()	// returns TIM2 counter register, works up to 70 minutes of processor on-time
{
	return TIM2->CNT;
}


/* TIM3 init function */
void TIM3_Init(void)
{
	RCC->APB1ENR |= RCC_APB1ENR_TIM3EN;	// enable timer 3 clock

	TIM3->CNT = (uint32_t)0x00000000;	// reset counter value
	TIM3->PSC = (uint16_t)((HAL_RCC_GetPCLK1Freq()*2 / 1000000) - 1);	// set prescaler so that it counts every microsecond
	TIM3->ARR = (uint32_t)(20000 - 1);	// set timer period (timer counts up to this number, then resets to 0 in up-counting mode) --> send a pulse every 20ms
	TIM3->CCER |= TIM_CCER_CC3E;	// enable output compare 3 output
	//TIM3->CCER |= TIM_CCER_CC3P;	// set polarity to active low
	//TIM3->CCMR2 |= TIM_CCMR2_OC3PE;	// enable preload on CCR3
	TIM3->CCMR2 |= 0b0110 << TIM_CCMR2_OC3M_Pos;	// set output compare 3 mode to pwm1

	buzzer_init();

	TIM3->CR1 |= TIM_CR1_CEN;	// enable timer3 counter
}


/* TIM5 init function */
void TIM5_Init(void)
{
	RCC->APB1ENR |= RCC_APB1ENR_TIM5EN;	// enable timer 5 clock

	TIM5->CNT = (uint32_t)0x00000000;	// reset counter value
	TIM5->PSC = (uint16_t)(HAL_RCC_GetPCLK1Freq()*2 / (DSHOT_PWM_FREQUENCY * DSHOT_PWM_RESOLUTION) - 1);	// set prescaler
	TIM5->ARR = (uint32_t)(DSHOT_PWM_RESOLUTION - 1);	// set timer period (timer counts up to this number, then resets to 0 in up-counting mode)
	TIM5->CCER |= TIM_CCER_CC1E | TIM_CCER_CC2E | TIM_CCER_CC3E | TIM_CCER_CC4E;	// enable output compare 1, 2, 3 and 4 outputs
	TIM5->CCMR1 |= TIM_CCMR1_OC1PE | TIM_CCMR1_OC2PE;	// enable preload on CCR1, CCR2, CCR3, CCR4
	TIM5->CCMR2 |= TIM_CCMR2_OC3PE | TIM_CCMR2_OC4PE;
	TIM5->CCMR1 |= (0b0110 << TIM_CCMR1_OC1M_Pos) | (0b0110 << TIM_CCMR1_OC2M_Pos);	// set output compare 1, 2 modes to pwm1
	TIM5->CCMR2 |= (0b0110 << TIM_CCMR2_OC3M_Pos) | (0b0110 << TIM_CCMR2_OC4M_Pos);	// set output compare 3, 4 modes to pwm1
	TIM5->DCR |= (DSHOT_NUM_MOTORS - 1) << TIM_DCR_DBL_Pos;	// DMA burst transfer setup
	TIM5->DCR |= (TIM5_CCR1_ADDRESS_OFFSET / 4) << TIM_DCR_DBA_Pos;		// DMA burst transfer setup

	TIM5->CCR1 = 0;	// set channel 1 pulse lenght to 0
	TIM5->CCR2 = 0; // set channel 2 pulse lenght to 0
	TIM5->CCR3 = 0;	// set channel 3 pulse lenght to 0
	TIM5->CCR4 = 0; // set channel 4 pulse lenght to 0

	TIM5->DIER |= TIM_DIER_TDE;	// enable timer5 DMA request trigger
	TIM5->DIER |= TIM_DIER_UDE;	// enable timer5 DMA request on update

}
