/*
 * ringBuf.c
 *
 *  Created on: Sep 25, 2020
 *      Author: lukas
 */


#include "ringBuf.h"
#include <assert.h>

#pragma mark - Private Functions -

static void advance_pointer(ringbuf_handle_t ringbuf)
{
	assert(ringbuf);

	if(ringbuf->full)
    {
        ringbuf->tail = (ringbuf->tail + 1) % ringbuf->max;
    }

	ringbuf->head = (ringbuf->head + 1) % ringbuf->max;

	// We mark full because we will advance tail on the next time around
	ringbuf->full = (ringbuf->head == ringbuf->tail);
}

static void retreat_pointer(ringbuf_handle_t ringbuf)
{
	assert(ringbuf);

	ringbuf->full = false;
	ringbuf->tail = (ringbuf->tail + 1) % ringbuf->max;
}

#pragma mark - APIs -

ringbuf_handle_t ringBuf_init(uint8_t* buffer, size_t size)
{
	assert(buffer && size);

	ringbuf_handle_t ringbuf = malloc(sizeof(ringBuf_t));
	assert(ringbuf);

	ringbuf->buffer = buffer;
	ringbuf->max = size;
	ringBuf_reset(ringbuf);

	assert(ringBuf_empty(ringbuf));

	return ringbuf;
}

void ringBuf_free(ringbuf_handle_t ringbuf)
{
	assert(ringbuf);
	free(ringbuf);
}

void ringBuf_reset(ringbuf_handle_t ringbuf)
{
    assert(ringbuf);

    ringbuf->head = 0;
    ringbuf->tail = 0;
    ringbuf->full = false;
}

size_t ringBuf_getSize(ringbuf_handle_t ringbuf)
{
	assert(ringbuf);

	size_t size = ringbuf->max;

	if(!ringbuf->full)
	{
		if(ringbuf->head >= ringbuf->tail)
		{
			size = (ringbuf->head - ringbuf->tail);
		}
		else
		{
			size = (ringbuf->max + ringbuf->head - ringbuf->tail);
		}

	}

	return size;
}

size_t ringBuf_getCapacity(ringbuf_handle_t ringbuf)
{
	assert(ringbuf);

	return ringbuf->max;
}

void ringBuf_put(ringbuf_handle_t ringbuf, uint8_t data)
{
	assert(ringbuf && ringbuf->buffer);

    ringbuf->buffer[ringbuf->head] = data;

    advance_pointer(ringbuf);
}

int ringBuf_put2(ringbuf_handle_t ringbuf, uint8_t data)
{
    int r = -1;

    assert(ringbuf && ringbuf->buffer);

    if(!ringBuf_full(ringbuf))
    {
        ringbuf->buffer[ringbuf->head] = data;
        advance_pointer(ringbuf);
        r = 0;
    }

    return r;
}

int ringBuf_get(ringbuf_handle_t ringbuf, uint8_t * data)
{
    assert(ringbuf && data && ringbuf->buffer);

    int r = -1;

    if(!ringBuf_empty(ringbuf))
    {
        *data = ringbuf->buffer[ringbuf->tail];
        retreat_pointer(ringbuf);

        r = 0;
    }

    return r;
}

bool ringBuf_empty(ringbuf_handle_t ringbuf)
{
	assert(ringbuf);

    return (!ringbuf->full && (ringbuf->head == ringbuf->tail));
}

bool ringBuf_full(ringbuf_handle_t ringbuf)
{
	assert(ringbuf);

    return ringbuf->full;
}
