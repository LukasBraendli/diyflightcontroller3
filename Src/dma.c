/**
  ******************************************************************************
  * File Name          : dma.c
  * Description        : This file provides code for the configuration
  *                      of all the requested memory to memory DMA transfers.
  ******************************************************************************
  * @attention
  *
  * <h2><center>&copy; Copyright (c) 2020 STMicroelectronics.
  * All rights reserved.</center></h2>
  *
  * This software component is licensed by ST under Ultimate Liberty license
  * SLA0044, the "License"; You may not use this file except in compliance with
  * the License. You may obtain a copy of the License at:
  *                             www.st.com/SLA0044
  *
  ******************************************************************************
  */

/* Includes ------------------------------------------------------------------*/
#include "dma.h"

/* USER CODE BEGIN 0 */
#include "dshot.h"
/* USER CODE END 0 */

/*----------------------------------------------------------------------------*/
/* Configure DMA                                                              */
/*----------------------------------------------------------------------------*/

/* USER CODE BEGIN 1 */

/* USER CODE END 1 */

/** 
  * Enable DMA controller clock
  */
void MX_DMA_Init(void) 
{

  /* DMA controller clock enable */
  __HAL_RCC_DMA1_CLK_ENABLE();

  /* DMA interrupt init */
  /* DMA1_Stream0_IRQn interrupt configuration */
  HAL_NVIC_SetPriority(DMA1_Stream0_IRQn, 0, 0);
  HAL_NVIC_EnableIRQ(DMA1_Stream0_IRQn);

}

/* USER CODE BEGIN 2 */
/**
 * DMA initialization for dshot protocol with timer 5
 */
/* DMA controller clock enable */
void CUSTOM_DMA_Init(void)
{

	// enable DMA1 clock
	__HAL_RCC_DMA1_CLK_ENABLE();

	// DMA1 Stream6; memory to tim5
	DMA1_Stream6->PAR = (uint32_t)&TIM5->DMAR;	// set peripheral (destination) address
	DMA1_Stream6->M0AR = (uint32_t)&dshot_cmd_buf;	// set memory (source) address
	DMA1_Stream6->NDTR = (uint16_t)(DSHOT_CMD_SIZE * DSHOT_NUM_MOTORS);	// set number of data items to be transferred
	DMA1_Stream6->CR = 0x00000000;	// reset control register
	DMA1_Stream6->CR |= 6 << DMA_SxCR_CHSEL_Pos;	// set channel to 6 (TIM5_UP)
	DMA1_Stream6->CR |= 2 << DMA_SxCR_PL_Pos;		// set priority level to 2 (High)
	DMA1_Stream6->CR |= 1 << DMA_SxCR_DIR_Pos;		// set data transfer direction bits to 1 (mem-to-periph)
	DMA1_Stream6->CR |= 1 << DMA_SxCR_MINC_Pos;		// enable memory pointer increment after transfer
	DMA1_Stream6->CR |= 3 << DMA_SxCR_MSIZE_Pos;	// set memory data size bits to 3 (word = 32bit)
	DMA1_Stream6->CR |= 3 << DMA_SxCR_PSIZE_Pos;	// set peripheral data size bits to 3 (word = 32bit)
	DMA1_Stream6->CR |= 1 << DMA_SxCR_TCIE_Pos; 	// enable transfer complete interrupt
	//DMA1_Stream6->CR &= ~(uint32_t)DMA_SxCR_HTIE;	// disable half transfer complete interrupt
/*
	// DMA1 Stream6; memory to tim5
	DMA1_Stream6->CR = 0x00000000;	// reset control register
	DMA1_Stream6->CR |= DMA_CHANNEL_6;				// set channel to 6 (TIM5_UP)
	DMA1_Stream6->CR |= DMA_PRIORITY_HIGH;			// set priority level to 2 (High)
	DMA1_Stream6->CR |= DMA_MEMORY_TO_PERIPH;		// set data transfer direction bits to 1 (mem-to-periph)
	DMA1_Stream6->CR |= DMA_PINC_DISABLE;			// disable peripheral pointer increment after transfer
	DMA1_Stream6->CR |= DMA_MINC_ENABLE;			// enable memory pointer increment after transfer
	DMA1_Stream6->CR |= DMA_MDATAALIGN_WORD;		// set memory data size bits to 3 (word = 32bit)
	DMA1_Stream6->CR |= DMA_PDATAALIGN_WORD;		// set peripheral data size bits to 3 (word = 32bit)
	DMA1_Stream6->CR |= DMA_IT_TC; 					// enable transfer complete interrupt
	//DMA1_Stream6->CR &= ~(uint32_t)DMA_SxCR_HTIE;	// disable half transfer complete interrupt

	DMA1_Stream6->NDTR = (uint16_t)(DSHOT_CMD_SIZE * DSHOT_NUM_MOTORS);	// set number of data items to be
																		// 		transferred in total
	DMA1_Stream6->PAR = (uint32_t)&TIM5->DMAR;		// set peripheral (destination) address
	DMA1_Stream6->M0AR = (uint32_t)dshot_cmd_buf;	// set memory (source) address
*/
	//DMA1_Stream6->CR |= DMA_SxCR_EN;	// enable DMA1 Stream6


	/* DMA interrupt init */
	/* DMA1_Stream6_IRQn interrupt configuration */
	HAL_NVIC_SetPriority(DMA1_Stream6_IRQn, 0, 0);
	HAL_NVIC_EnableIRQ(DMA1_Stream6_IRQn);
}
/* USER CODE END 2 */

/**
  * @}
  */

/**
  * @}
  */

/************************ (C) COPYRIGHT STMicroelectronics *****END OF FILE****/
