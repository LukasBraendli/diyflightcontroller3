/*
 * sbus.c
 *
 *  Created on: 20.08.2018
 *      Author: IchWerSonst
 */

/* Includes ------------------------------------------------------------------*/
#include "sbus.h"
#include "timers_custom.h"


/* Functions ------------------------------------------------------------------*/
SBUS_t sbus_begin(UART_HandleTypeDef *huart)
{
	SBUS_t sbus;
	sbus.huart = huart;
	for(uint8_t i = 0; i < SBUS_CHANNEL_NUMBER; i++)
	{
		sbus.channels[i] = 0;	// initialize channel data with zero
	}
	sbus.framelost_flag = 0;
	sbus.rx_failsave_flag = 0;
	sbus.fc_failsave_flag = 0;
	sbus.failsave_timer_ms = HAL_GetTick();

	while(HAL_UART_Receive_DMA(huart, (uint8_t *)sbus_RXbuf, SBUS_RX_SIZE) != HAL_OK);	// enable DMA1_Stream0

	return sbus;
}


void sbus_uartRxCplt(UART_HandleTypeDef *huart, SBUS_t *sbus)
{
	if(huart == sbus->huart)
	{
		sbus_updateFlag = 1;
		sbus->failsave_timer_ms = HAL_GetTick();
	}
}


void sbus_clearUpdateFlag(void)
{
	sbus_updateFlag = 0;
}


int8_t sbus_getChannels(SBUS_t *sbus)
{
	int8_t counter = 0;
	do
	{
		counter++;
		sbus_updateFlag = 0;	// clear flag

		// see futaba SBUS documentation
		sbus->channels[0]  = ((sbus_RXbuf[0+SBUS_CHANNEL_OFFSET]    |sbus_RXbuf[1+SBUS_CHANNEL_OFFSET]<<8)											& 0x07FF);
		sbus->channels[1]  = ((sbus_RXbuf[1+SBUS_CHANNEL_OFFSET]>>3 |sbus_RXbuf[2+SBUS_CHANNEL_OFFSET]<<5)											& 0x07FF);
		sbus->channels[2]  = ((sbus_RXbuf[2+SBUS_CHANNEL_OFFSET]>>6 |sbus_RXbuf[3+SBUS_CHANNEL_OFFSET]<<2 |sbus_RXbuf[4+SBUS_CHANNEL_OFFSET]<<10)	& 0x07FF);
		sbus->channels[3]  = ((sbus_RXbuf[4+SBUS_CHANNEL_OFFSET]>>1 |sbus_RXbuf[5+SBUS_CHANNEL_OFFSET]<<7)											& 0x07FF);
		sbus->channels[4]  = ((sbus_RXbuf[5+SBUS_CHANNEL_OFFSET]>>4 |sbus_RXbuf[6+SBUS_CHANNEL_OFFSET]<<4)											& 0x07FF);
		sbus->channels[5]  = ((sbus_RXbuf[6+SBUS_CHANNEL_OFFSET]>>7 |sbus_RXbuf[7+SBUS_CHANNEL_OFFSET]<<1 |sbus_RXbuf[8+SBUS_CHANNEL_OFFSET]<<9)		& 0x07FF);
		sbus->channels[6]  = ((sbus_RXbuf[8+SBUS_CHANNEL_OFFSET]>>2 |sbus_RXbuf[9+SBUS_CHANNEL_OFFSET]<<6)											& 0x07FF);
		sbus->channels[7]  = ((sbus_RXbuf[9+SBUS_CHANNEL_OFFSET]>>5|sbus_RXbuf[10+SBUS_CHANNEL_OFFSET]<<3)											& 0x07FF);
		sbus->channels[8]  = ((sbus_RXbuf[11+SBUS_CHANNEL_OFFSET]   |sbus_RXbuf[12+SBUS_CHANNEL_OFFSET]<<8)										& 0x07FF);
		sbus->channels[9]  = ((sbus_RXbuf[12+SBUS_CHANNEL_OFFSET]>>3|sbus_RXbuf[13+SBUS_CHANNEL_OFFSET]<<5)										& 0x07FF);
		sbus->channels[10] = ((sbus_RXbuf[13+SBUS_CHANNEL_OFFSET]>>6|sbus_RXbuf[14+SBUS_CHANNEL_OFFSET]<<2|sbus_RXbuf[15+SBUS_CHANNEL_OFFSET]<<10)	& 0x07FF);
		sbus->channels[11] = ((sbus_RXbuf[15+SBUS_CHANNEL_OFFSET]>>1|sbus_RXbuf[16+SBUS_CHANNEL_OFFSET]<<7)										& 0x07FF);
		sbus->channels[12] = ((sbus_RXbuf[16+SBUS_CHANNEL_OFFSET]>>4|sbus_RXbuf[17+SBUS_CHANNEL_OFFSET]<<4)										& 0x07FF);
		sbus->channels[13] = ((sbus_RXbuf[17+SBUS_CHANNEL_OFFSET]>>7|sbus_RXbuf[18+SBUS_CHANNEL_OFFSET]<<1|sbus_RXbuf[19+SBUS_CHANNEL_OFFSET]<<9)	& 0x07FF);
		sbus->channels[14] = ((sbus_RXbuf[19+SBUS_CHANNEL_OFFSET]>>2|sbus_RXbuf[20+SBUS_CHANNEL_OFFSET]<<6)										& 0x07FF);
		sbus->channels[15] = ((sbus_RXbuf[20+SBUS_CHANNEL_OFFSET]>>5|sbus_RXbuf[21+SBUS_CHANNEL_OFFSET]<<3)										& 0x07FF);

		sbus->channels[17] = (sbus_RXbuf[22+SBUS_CHANNEL_OFFSET] & 0b10000000) ? 2047 : 0;
		sbus->channels[18] = (sbus_RXbuf[22+SBUS_CHANNEL_OFFSET] & 0b01000000) ? 2047 : 0;
		sbus->framelost_flag = (sbus_RXbuf[22+SBUS_CHANNEL_OFFSET] & 0b00100000) ? 1 : 0;
		sbus->rx_failsave_flag = (sbus_RXbuf[22+SBUS_CHANNEL_OFFSET] & 0b00010000) ? SBUS_FAILSAFE_ACTIVE : SBUS_FAILSAFE_INACTIVE;

	}
	while(sbus_updateFlag);	// if sbus data has been updated by DMA during this function repeat to prevent data corruption

	return counter;
}


/**
 * checks whether the failsave timer is higher than the failsave timeout
 * @param sbus the SBUS instance
 * @retval returns SBUS_FAILSAVE_ACTIVE iff failsave is active, i.e. if
 * 		- failsave_timer_ms exceeds SBUS_FAILSAVE_TIMEOUT_MS or if
 * 		- the rx_failsave_flag is set
 * 		else returns SBUS_FAILSAVE_INACTIVE
 */
uint8_t sbus_isFailsaveActive(SBUS_t *sbus)
{
	uint32_t now = HAL_GetTick();
	if((now - sbus->failsave_timer_ms) > SBUS_FAILSAVE_TIMEOUT_MS)
	{
		sbus->fc_failsave_flag = SBUS_FAILSAFE_ACTIVE;
	}
	else
	{
		sbus->fc_failsave_flag = SBUS_FAILSAFE_INACTIVE;
	}


	if((sbus->rx_failsave_flag == SBUS_FAILSAFE_ACTIVE) || (sbus->fc_failsave_flag == SBUS_FAILSAFE_ACTIVE))
	{
		return SBUS_FAILSAFE_ACTIVE;
	}

	return SBUS_FAILSAFE_INACTIVE;
}
