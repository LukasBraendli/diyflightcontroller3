/*
 * filters.c
 *
 *  Created on: Aug 13, 2020
 *      Author: lukas
 */

#include "filters.h"
#include <stdlib.h>	// for easy struct allocating


biquad_t *biquad_init(const double b[3], const double a[3])
{
	biquad_t *bq = malloc(sizeof *bq);
	bq->b0 = b[0];
	bq->b1 = b[1];
	bq->b2 = b[2];

	bq->a1 = a[1];
	bq->a2 = a[2];

	bq->d1 = 0;
	bq->d2 = 0;

	return bq;
}


double biquad_evaluate(biquad_t *bq, double in)
{
	double out = bq->b0*in + bq->d1;
	bq->d1 = bq->b1*in - bq->a1*out + bq->d2;
	bq->d2 = bq->b2*in - bq->a2*out;

	return out;
}


filter_t *filter_init(filterType_t order)
{
	filter_t *filter = malloc(sizeof *filter);
	biquad_t *next = NULL;

	switch(order)
	{
		case iir_lpf_o10_Fs1000_Fc100:
			filter->order = 10;
			filter->bq_list = biquad_init(b_iir_o10_Fs1000_Fc100[0], a_iir_o10_Fs1000_Fc100[0]);
			next = filter->bq_list;
			for(int i = 1; i < 5; i++)
			{
				next->next = biquad_init(b_iir_o10_Fs1000_Fc100[i], a_iir_o10_Fs1000_Fc100[i]);
				next = next->next;
			}
			next->next = NULL;
		break;

		case iir_lpf_o10_Fs1000_Fc80:
			filter->order = 10;
			filter->bq_list = biquad_init(b_iir_o10_Fs1000_Fc80[0], a_iir_o10_Fs1000_Fc80[0]);
			next = filter->bq_list;
			for(int i = 1; i < 5; i++)
			{
				next->next = biquad_init(b_iir_o10_Fs1000_Fc80[i], a_iir_o10_Fs1000_Fc80[i]);
				next = next->next;
			}
			next->next = NULL;
		break;

		case iir_lpf_o10_Fs1000_Fc60:
			filter->order = 10;
			filter->bq_list = biquad_init(b_iir_o10_Fs1000_Fc60[0], a_iir_o10_Fs1000_Fc60[0]);
			next = filter->bq_list;
			for(int i = 1; i < 5; i++)
			{
				next->next = biquad_init(b_iir_o10_Fs1000_Fc60[i], a_iir_o10_Fs1000_Fc60[i]);
				next = next->next;
			}
			next->next = NULL;
		break;

		default:
			filter->order = 0;
			filter->bq_list = NULL;
	}
	return filter;
}


double iir_evaluate(filter_t *filter, double in)
{
	double out = in;
	biquad_t *next = filter->bq_list;
	while(next != NULL)
	{
		out = biquad_evaluate(next, out);
		next = next->next;
	}
	return out;
}
