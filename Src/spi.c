/**
  ******************************************************************************
  * File Name          : SPI.c
  * Description        : This file provides code for the configuration
  *                      of the SPI instances.
  ******************************************************************************
  * @attention
  *
  * <h2><center>&copy; Copyright (c) 2020 STMicroelectronics.
  * All rights reserved.</center></h2>
  *
  * This software component is licensed by ST under Ultimate Liberty license
  * SLA0044, the "License"; You may not use this file except in compliance with
  * the License. You may obtain a copy of the License at:
  *                             www.st.com/SLA0044
  *
  ******************************************************************************
  */

/* Includes ------------------------------------------------------------------*/
#include "spi.h"

/* USER CODE BEGIN 0 */
#include <string.h>

/* USER CODE END 0 */

SPI_HandleTypeDef hspi2;

/* SPI2 init function */
void MX_SPI2_Init(void)
{

  hspi2.Instance = SPI2;
  hspi2.Init.Mode = SPI_MODE_MASTER;
  hspi2.Init.Direction = SPI_DIRECTION_2LINES;
  hspi2.Init.DataSize = SPI_DATASIZE_8BIT;
  hspi2.Init.CLKPolarity = SPI_POLARITY_LOW;
  hspi2.Init.CLKPhase = SPI_PHASE_1EDGE;
  hspi2.Init.NSS = SPI_NSS_SOFT;
  hspi2.Init.BaudRatePrescaler = SPI_BAUDRATEPRESCALER_8;
  hspi2.Init.FirstBit = SPI_FIRSTBIT_MSB;
  hspi2.Init.TIMode = SPI_TIMODE_DISABLE;
  hspi2.Init.CRCCalculation = SPI_CRCCALCULATION_DISABLE;
  hspi2.Init.CRCPolynomial = 7;
  hspi2.Init.CRCLength = SPI_CRC_LENGTH_DATASIZE;
  hspi2.Init.NSSPMode = SPI_NSS_PULSE_ENABLE;
  if (HAL_SPI_Init(&hspi2) != HAL_OK)
  {
    Error_Handler();
  }

}

void HAL_SPI_MspInit(SPI_HandleTypeDef* spiHandle)
{

  GPIO_InitTypeDef GPIO_InitStruct = {0};
  if(spiHandle->Instance==SPI2)
  {
  /* USER CODE BEGIN SPI2_MspInit 0 */

  /* USER CODE END SPI2_MspInit 0 */
    /* SPI2 clock enable */
    __HAL_RCC_SPI2_CLK_ENABLE();
  
    __HAL_RCC_GPIOC_CLK_ENABLE();
    __HAL_RCC_GPIOB_CLK_ENABLE();
    /**SPI2 GPIO Configuration    
    PC2     ------> SPI2_MISO
    PC3     ------> SPI2_MOSI
    PB13     ------> SPI2_SCK 
    */
    GPIO_InitStruct.Pin = GPIO_PIN_2|GPIO_PIN_3;
    GPIO_InitStruct.Mode = GPIO_MODE_AF_PP;
    GPIO_InitStruct.Pull = GPIO_NOPULL;
    GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_VERY_HIGH;
    GPIO_InitStruct.Alternate = GPIO_AF5_SPI2;
    HAL_GPIO_Init(GPIOC, &GPIO_InitStruct);

    GPIO_InitStruct.Pin = GPIO_PIN_13;
    GPIO_InitStruct.Mode = GPIO_MODE_AF_PP;
    GPIO_InitStruct.Pull = GPIO_NOPULL;
    GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_VERY_HIGH;
    GPIO_InitStruct.Alternate = GPIO_AF5_SPI2;
    HAL_GPIO_Init(GPIOB, &GPIO_InitStruct);

  /* USER CODE BEGIN SPI2_MspInit 1 */

  /* USER CODE END SPI2_MspInit 1 */
  }
}

void HAL_SPI_MspDeInit(SPI_HandleTypeDef* spiHandle)
{

  if(spiHandle->Instance==SPI2)
  {
  /* USER CODE BEGIN SPI2_MspDeInit 0 */

  /* USER CODE END SPI2_MspDeInit 0 */
    /* Peripheral clock disable */
    __HAL_RCC_SPI2_CLK_DISABLE();
  
    /**SPI2 GPIO Configuration    
    PC2     ------> SPI2_MISO
    PC3     ------> SPI2_MOSI
    PB13     ------> SPI2_SCK 
    */
    HAL_GPIO_DeInit(GPIOC, GPIO_PIN_2|GPIO_PIN_3);

    HAL_GPIO_DeInit(GPIOB, GPIO_PIN_13);

  /* USER CODE BEGIN SPI2_MspDeInit 1 */

  /* USER CODE END SPI2_MspDeInit 1 */
  }
} 

/* USER CODE BEGIN 1 */
usr_SPI_t usr_SPI_init(SPI_HandleTypeDef *hspi, GPIO_TypeDef *NSS_Port, uint16_t NSS_Pin, uint32_t Timeout_ms)
{
	usr_SPI_t usr_SPI;
	usr_SPI.hspi = hspi;
	usr_SPI.NSS_Port = NSS_Port;
	usr_SPI.NSS_Pin = NSS_Pin;
	usr_SPI.Timeout_ms = Timeout_ms;

	return usr_SPI;
}


void SPIWrite(usr_SPI_t *usr_SPI, uint8_t *cmd, uint16_t cmdLen)
{
	HAL_GPIO_WritePin(usr_SPI->NSS_Port, usr_SPI->NSS_Pin, 0);
	HAL_SPI_Transmit(usr_SPI->hspi, cmd, cmdLen, usr_SPI->Timeout_ms);
	HAL_GPIO_WritePin(usr_SPI->NSS_Port, usr_SPI->NSS_Pin, 1);
}


/*
 * for sending 1 or more (command) bytes directly after each other and after every command byte receiving an answer byte
 *
 * command/sent:	byte0|byte1|byte2| ... |byteN|-----
 * data/received:	-----|byte0|byte1|byte2| ... |byteN
 *
 * NOTE: "cmd" and "data" should be the same length; "data" must NEVER be shorter than "cmd"; "dataLen" should be the number of bytes in "cmd" and "data" respectively
 */
void SPIRead(usr_SPI_t *usr_SPI, uint8_t * cmd, uint8_t * data, uint16_t dataLen)
{
	uint8_t tempCmd[dataLen + 1];
	memcpy(tempCmd, cmd, dataLen);
	uint8_t tempData[dataLen + 1];
	HAL_GPIO_WritePin(usr_SPI->NSS_Port, usr_SPI->NSS_Pin, 0);
	HAL_SPI_TransmitReceive(usr_SPI->hspi, cmd, tempData, dataLen + 1, usr_SPI->Timeout_ms);
	HAL_GPIO_WritePin(usr_SPI->NSS_Port, usr_SPI->NSS_Pin, 1);

	memcpy(data, tempData + 1, dataLen);
}


/*
 * for sending 1 byte and immediately after receiving 1 or more bytes
 *
 * command/sent:	byte0|-----|-----|-----|-----|-----
 * data/received:	-----|byte0|byte1|byte2| ... |byteN
 */
void SPIBurstRead(usr_SPI_t *usr_SPI, uint8_t * cmd, uint8_t * data, uint16_t dataLen)
{
	HAL_GPIO_WritePin(usr_SPI->NSS_Port, usr_SPI->NSS_Pin, 0);
	HAL_SPI_Transmit(usr_SPI->hspi, cmd, 1, usr_SPI->Timeout_ms);
	HAL_SPI_Receive(usr_SPI->hspi, data, dataLen, usr_SPI->Timeout_ms);
	HAL_GPIO_WritePin(usr_SPI->NSS_Port, usr_SPI->NSS_Pin, 1);
}
/* USER CODE END 1 */

/************************ (C) COPYRIGHT STMicroelectronics *****END OF FILE****/
