/*
 * useful_functions.c
 *
 *  Created on: 11.08.2018
 *      Author: IchWerSonst
 */

#include "useful_functions.h"


long map(long x, long in_min, long in_max, long out_min, long out_max)
{
	return (x - in_min) * (out_max - out_min) / (in_max - in_min) + out_min;
}


double map_float(double x, double in_min, double in_max, double out_min, double out_max)
{
	return (x - in_min) * (out_max - out_min) / (in_max - in_min) + out_min;
}


long constrain(long x, long min, long max)
{
	if(x < min)
	{
		return min;
	}
	else if(x > max)
	{
		return max;
	}
	else
	{
		return x;
	}
}


double constrain_float(double x, double min, double max)
{
	if(x < min)
	{
		return min;
	}
	else if(x > max)
	{
		return max;
	}
	else
	{
		return x;
	}
}


/**
 * @brief this function checks whether a specified time period (in milliseconds)
 * 		has elapsed since the last function call that returned true
 * @param interval_ms the time period in milliseconds
 * @retval 1 iff the specified amount of time or more has elapsed since the last time "true" was returned; 0 otherwise
 */
uint8_t update(uint32_t interval_ms)
{
	static uint32_t last_time = 0;
	if(last_time == 0)
	{
		last_time = HAL_GetTick();
		return 0;
	}

	uint32_t now = HAL_GetTick();
	if(now >= last_time + interval_ms)
	{
		last_time = now;
		return 1;
	}
	return 0;
}
