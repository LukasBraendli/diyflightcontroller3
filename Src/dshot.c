/*
 * dshot.c
 *
 *  Created on: 31.07.2018
 *      Author: IchWerSonst
 */

#include "dshot.h"
#include <string.h>


/* Functions ------------------------------------------------------------------*/
void dshot_init(void)
{
	dshot_clearBusyFlag();
}

uint8_t dshot_getChecksum(uint16_t value)
{
	uint8_t checksum = 0;
	for(uint8_t i = 0; i < 3; i++)
	{
		checksum ^= value;
		value >>=  4;
	}
	checksum &= 0x0f;

	return checksum;
}


/**
 * @brief generates from the motor values the data for the dshot timer fed by the dma
 * @param motor_values pointer to array containing the throttle values for each motor
 * @param _telemetry_request array with a bool for every motor; if 0 --> send NO telemetry request
 * 		else send request
 * @param len size of the motor_values array and telemetry_request array <=> ideally number of motors
 * @retval returns -1 if len is NOT equal to DSHOT_NUM_MOTORS (in "dshot.h"); else returns 0
 */
int8_t dshot_generateCmd(uint16_t *motor_values, uint8_t *_telemetry_request, uint16_t len)
{
	if(len != DSHOT_NUM_MOTORS)
	{
		return -1;
	}

	uint8_t telemetry_request[DSHOT_NUM_MOTORS];
	uint16_t motor_commands[DSHOT_NUM_MOTORS];
	for(uint8_t m = 0; m < DSHOT_NUM_MOTORS; m++)
	{
		telemetry_request[m] = (_telemetry_request[m] > 0);	// ensure that the value of telemetry_request[m] is either 1 or 0, no other number
		motor_commands[m] = (motor_values[m] << 1) | telemetry_request[m];	// add telemetry request bit to command
		motor_commands[m] = (motor_commands[m] << 4) | dshot_getChecksum(motor_commands[m]);	// add 4 checksum bits to command
	}


	/* fill single buffers with command values */
	uint32_t tmp_cmd_buf[DSHOT_NUM_MOTORS][DSHOT_CMD_SIZE];	// temporary command buffer for every single motor

	for(uint8_t m = 0; m < DSHOT_NUM_MOTORS; m++)
	{
		for(uint8_t i = 0; i < DSHOT_CMD_SIZE; i++)	// set buffer content to 0
		{
			tmp_cmd_buf[m][i] = (uint32_t)0;
		}

		for(uint16_t i = 0; i < (DSHOT_CMD_SIZE - 1); i++)
		{
			if((uint16_t)((1 << i) & motor_commands[m]))	// mask off single bits and check whether they are high or low
			{
				tmp_cmd_buf[m][15 - i] = DSHOT_PWM_HIGH;	// fill buffer with number of timer ticks corresponding to 2/3 pwm duty cycle = dshot high bit
			}
			else
			{
				tmp_cmd_buf[m][15 - i] = DSHOT_PWM_LOW;		// fill buffer with number of timer ticks corresponding to 1/3 pwm duty cycle = dshot low bit
			}
		}
	}


	/* fuse those buffers to one command buffer */
	for(uint16_t i = 0; i < DSHOT_CMD_SIZE * DSHOT_NUM_MOTORS; i++)	// set buffer content to 0
	{
		dshot_cmd_buf[i] = (uint32_t)0;
	}

	for(uint16_t i = 0; i < DSHOT_CMD_SIZE * DSHOT_NUM_MOTORS; i += DSHOT_NUM_MOTORS)	// fill dshot command buffer
	{
		for(uint8_t m = 0; m < DSHOT_NUM_MOTORS; m++)
		{
			dshot_cmd_buf[i + m] = tmp_cmd_buf[m][i/DSHOT_NUM_MOTORS];
		}
	}

	return 0;
}


/**
 * @brief initiates dma transfer for dshot from memory to timer
 */
void dshot_sendAll(void)
{
	dshot_setBusyFlag();
	DMA1_Stream6->CR |= DMA_SxCR_EN;	// enable DMA1_Stream6
	TIM5->CR1 |= TIM_CR1_CEN;	// enable timer5 counter
}

/**
 * @brief sets the dshot_busy_flag variable to 1 (busy)
 */
void dshot_setBusyFlag(void)
{
	dshot_busy_flag = 1;
}


/**
 * @brief clears the dshot_busy_flag variable to 0 (NOT busy)
 */
void dshot_clearBusyFlag(void)
{
	dshot_busy_flag = 0;
}


/**
 * @brief returns the dshot_busy_flag variable
 * @retval returns either 0 (NOT busy) or 1 (busy)
 */
uint8_t dshot_isBusy(void)
{
	return dshot_busy_flag;
}


/**
 * @brief manages dma transfer complete interupts
 * 		--> clears dshot_busy_flag
 * 		--> disables dshot timer
 * 		--> resets dshot timer counter
 */
void dshot_dmaTransferCpltHandler(void)
{
	dshot_clearBusyFlag();
	TIM5->CR1 &= ~TIM_CR1_CEN;	// disable timer5 counter
	TIM5->CNT = (uint32_t)0x00000000;	// reset counter value
}
