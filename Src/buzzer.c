/*
 * buzzer.c
 *
 *  Created on: 05.06.2020
 *      Author: IchWerSonst
 */


#include "buzzer.h"


/**
 * initializes the buzzer functionalities
 * - set up timer 2 channel 4
 */
void buzzer_init(void)
{
	TIM3->CCR3 = BUZZER_OFF_VALUE;	// set channel 3 pulse lenght to the value for buzzer off
	buzzer_status = BUZZER_OFF_VALUE;
}



//void beep(uint16_t on_ms, uint16_t off_ms);


/**
 * @brief enables the PDB Buzzer
 * sets the pwm duty cycle of timer3 channel3 to BUZZER_ON_VALUE
 */
void buzzerOn(void)
{
	//TIM3->CR1 &= ~TIM_CR1_CEN;	// disable timer3 counter
	//TIM3->CNT = (uint32_t)0x00000000;	// reset counter value
	TIM3->CCR3 = BUZZER_ON_VALUE;	// set channel 3 pulse lenght to the value for buzzer on
	buzzer_status = BUZZER_ON_VALUE;
	//TIM3->CR1 |= TIM_CR1_CEN;	// reenable timer3 counter
}

/**
 * @brief diables the PDB Buzzer
 * sets the pwm duty cycle of timer3 channel3 to BUZZER_OFF_VALUE
 */
void buzzerOff(void)
{
	//TIM3->CR1 &= ~TIM_CR1_CEN;	// disable timer3 counter
	//TIM3->CNT = (uint32_t)0x00000000;	// reset counter value
	TIM3->CCR3 = BUZZER_OFF_VALUE;	// set channel 3 pulse lenght to the value for buzzer off
	buzzer_status = BUZZER_OFF_VALUE;
	//TIM3->CR1 |= TIM_CR1_CEN;	// reenable timer3 counter
}

void toggleBuzzer(void)
{
	if(buzzer_status == BUZZER_OFF_VALUE)
	{
		buzzerOn();
	}
	else if(buzzer_status == BUZZER_ON_VALUE)
	{
		buzzerOff();
	}
}



/*void buzzer_scheduler(void)
{
	//TIM3->CR1 &= ~TIM_CR1_CEN;	// disable timer3 counter
	//TIM3->CNT = (uint32_t)0x00000000;	// reset counter value
	TIM3->CCR3 = BUZZER_ON_VALUE;	// set channel 3 pulse lenght to the value for buzzer on
	//TIM3->CR1 |= TIM_CR1_CEN;	// reenable timer3 counter

	//TIM3->CR1 &= ~TIM_CR1_CEN;	// disable timer3 counter
	//TIM3->CNT = (uint32_t)0x00000000;	// reset counter value
	TIM3->CCR3 = BUZZER_OFF_VALUE;	// set channel 3 pulse lenght to the value for buzzer off
	//TIM3->CR1 |= TIM_CR1_CEN;	// reenable timer3 counter
}*/
