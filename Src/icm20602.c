/*
 * icm20602.c
 *
 *  Created on: 08.07.2018
 *      Author: IchWerSonst
 */

/* Includes ------------------------------------------------------------------*/
#include "icm20602.h"
#include "main.h"	// for pin definitions
#include "useful_functions.h"


/* Vairables -----------------------------------------------------------------*/


/* Functions -----------------------------------------------------------------*/
void icm20602_EXTIHandler(uint16_t GPIO_Pin)
{
	if(GPIO_Pin == ICM20602_INT_Pin)	// if interrupted pin is from icm20602
	{
		icm20602_intFlag = 1;	// set the gyro interrupt flag
	}
}


void icm20602_clearIntFlag()
{
	icm20602_intFlag = 0;
}


uint8_t icm20602_whoAmI(usr_SPI_t *usr_SPI)
{
	uint8_t data = 0;
	uint8_t cmd = ICM20602_WHO_AM_I | (ICM20602_SPI_Read_Bit << 7);
	SPIBurstRead(usr_SPI, &cmd, &data, 1);

	return data;
}


uint8_t icm20602_testConnection(usr_SPI_t *usr_SPI)
{
	return (icm20602_whoAmI(usr_SPI) == 18);
}


void icm20602_reset(usr_SPI_t *usr_SPI)
{
	uint8_t cmd[2] = {ICM20602_PWR_MGMT_1 | (ICM20602_SPI_Write_Bit << 7), 0b10000000};
	SPIWrite(usr_SPI, cmd, 2);
}


void icm20602_init(usr_SPI_t *usr_SPI)
{
	//set PWR_MGMT_1 to 1 to wake up chip and auto select clock source
	uint8_t cmd[10];
	uint8_t data[10];
	cmd[0] = ICM20602_PWR_MGMT_1 | (ICM20602_SPI_Write_Bit << 7);
	cmd[1] = 0x01;
	SPIWrite(usr_SPI, cmd, 2);

	//set I2C_IF bit 6 to enable SPI mode only
	cmd[0] = ICM20602_I2C_IF | (ICM20602_SPI_Read_Bit << 7);
	cmd[1] = 0;
	SPIBurstRead(usr_SPI, cmd, data, 1);
	cmd[0] = ICM20602_I2C_IF | (ICM20602_SPI_Write_Bit << 7);
	cmd[1] = data[0] | 0b01000000;
	SPIWrite(usr_SPI, cmd, 2);

	//set ACCEL_INTEL_CTRL bit 1; see ICM-20602 datasheet ACCEL_INTEL_CTRL register
	cmd[0] = ICM20602_ACCEL_INTEL_CTRL | (ICM20602_SPI_Read_Bit << 7);
	cmd[1] = 0;
	SPIBurstRead(usr_SPI, cmd, data, 1);
	cmd[0] = ICM20602_ACCEL_INTEL_CTRL | (ICM20602_SPI_Write_Bit << 7);
	cmd[1] = data[0] | 0b00000010;
	SPIWrite(usr_SPI, cmd, 2);

	// select DLPF configuraton 1 (CONFIG[2:0]); see ICM-20602 datasheet CONFIG register
	cmd[0] = ICM20602_CONFIG | (ICM20602_SPI_Write_Bit << 7);
	cmd[1] = 0b00000001;
	SPIWrite(usr_SPI, cmd, 2);

	//set Gyro Full Scale range to default +/-2000dps
	icm20602_setGyroFS(usr_SPI, ICM20602_GYRO_FS_2000);

	//set Accel Full Scale range to default +/-8g
	icm20602_setAccelFS(usr_SPI, ICM20602_ACCEL_FS_8);

}


uint8_t icm20602_getGyroFS(usr_SPI_t *usr_SPI)
{
	uint8_t cmd = ICM20602_GYRO_CONFIG | (ICM20602_SPI_Read_Bit << 7);
	uint8_t data = 0;
	SPIBurstRead(usr_SPI, &cmd, &data, 1);
	return data;
}


void icm20602_setGyroFS(usr_SPI_t *usr_SPI, uint8_t GyroFS)
{
	uint8_t cmd[2] = {ICM20602_GYRO_CONFIG | (ICM20602_SPI_Read_Bit << 7), 0};
	uint8_t data = 0;
	SPIBurstRead(usr_SPI, cmd, &data, 1);

	cmd[0] = ICM20602_GYRO_CONFIG | (ICM20602_SPI_Write_Bit << 7);
	cmd[1] = data | (GyroFS << 3);
	SPIWrite(usr_SPI, cmd, 2);

	icm20602_gyroFS = GyroFS;
}


uint8_t icm20602_getAccelFS(usr_SPI_t *usr_SPI)
{
	uint8_t cmd = ICM20602_ACCEL_CONFIG | (ICM20602_SPI_Read_Bit << 7);
	uint8_t data = 0;
	SPIBurstRead(usr_SPI, &cmd, &data, 1);
	return data;
}


void icm20602_setAccelFS(usr_SPI_t *usr_SPI, uint8_t AccelFS)
{
	uint8_t cmd[2] = {ICM20602_ACCEL_CONFIG | (ICM20602_SPI_Read_Bit << 7), 0};
	uint8_t data = 0;
	SPIBurstRead(usr_SPI, cmd, &data, 1);

	cmd[0] = ICM20602_ACCEL_CONFIG | (ICM20602_SPI_Write_Bit << 7);
	cmd[1] = data | (AccelFS << 3);
	SPIWrite(usr_SPI, cmd, 2);
}


int16_t icm20602_getXGyroOffset(usr_SPI_t *usr_SPI)
{
	uint8_t cmd[] = {ICM20602_XG_OFFS_USRH | (ICM20602_SPI_Read_Bit << 7), ICM20602_XG_OFFS_USRL | (ICM20602_SPI_Read_Bit << 7)};
	uint8_t data[2];
	SPIRead(usr_SPI, cmd, data, 2);

	return (((int16_t)data[0]) << 8) | data[1];
}


int16_t icm20602_getYGyroOffset(usr_SPI_t *usr_SPI)
{
	uint8_t cmd[] = {ICM20602_YG_OFFS_USRH | (ICM20602_SPI_Read_Bit << 7), ICM20602_YG_OFFS_USRL | (ICM20602_SPI_Read_Bit << 7)};
	uint8_t data[2];
	SPIRead(usr_SPI, cmd, data, 2);

	return (((int16_t)data[0]) << 8) | data[1];
}


int16_t icm20602_getZGyroOffset(usr_SPI_t *usr_SPI)
{
	uint8_t cmd[] = {ICM20602_ZG_OFFS_USRH + (ICM20602_SPI_Read_Bit << 7), ICM20602_ZG_OFFS_USRL + (ICM20602_SPI_Read_Bit << 7)};
	uint8_t data[2];
	SPIRead(usr_SPI, cmd, data, 2);

	return (((int16_t)data[0]) << 8) | data[1];
}


void icm20602_setGyroOffset(usr_SPI_t *usr_SPI, int16_t XOffset, int16_t YOffset, int16_t ZOffset)
{
	uint8_t cmd[] = {
			ICM20602_XG_OFFS_USRH | (ICM20602_SPI_Write_Bit << 7),	// base address of gyro offsets
			(uint8_t)(XOffset >> 8), (uint8_t)(XOffset),
			(uint8_t)(YOffset >> 8), (uint8_t)(YOffset),
			(uint8_t)(ZOffset >> 8), (uint8_t)(ZOffset)
	};
	SPIWrite(usr_SPI, cmd, sizeof(cmd));
}


int16_t icm20602_getXAccelOffset(usr_SPI_t *usr_SPI)
{
	uint8_t cmd[] = {ICM20602_XA_OFFSET_H | (ICM20602_SPI_Read_Bit << 7), ICM20602_XA_OFFSET_L | (ICM20602_SPI_Read_Bit << 7)};
	uint8_t data[2];
	SPIRead(usr_SPI, cmd, data, 2);

	return (((int16_t)data[0]) << 8) | data[1];
}


int16_t icm20602_getYAccelOffset(usr_SPI_t *usr_SPI)
{
	uint8_t cmd[] = {ICM20602_YA_OFFSET_H | (ICM20602_SPI_Read_Bit << 7), ICM20602_YA_OFFSET_L | (ICM20602_SPI_Read_Bit << 7)};
	uint8_t data[2];
	SPIRead(usr_SPI, cmd, data, 2);

	return (((int16_t)data[0]) << 8) | data[1];
}


int16_t icm20602_getZAccelOffset(usr_SPI_t *usr_SPI)
{
	uint8_t cmd[] = {ICM20602_ZA_OFFSET_H + (ICM20602_SPI_Read_Bit << 7), ICM20602_ZA_OFFSET_L + (ICM20602_SPI_Read_Bit << 7)};
	uint8_t data[2];
	SPIRead(usr_SPI, cmd, data, 2);

	return (((int16_t)data[0]) << 8) | data[1];
}


void icm20602_setAccelOffset(usr_SPI_t *usr_SPI, int16_t XOffset, int16_t YOffset, int16_t ZOffset)
{
	uint8_t Xcmd[3] = {ICM20602_XA_OFFSET_H | (ICM20602_SPI_Write_Bit << 7), (uint8_t)(XOffset >> 8), (uint8_t)(XOffset)};
	SPIWrite(usr_SPI, Xcmd, sizeof(Xcmd));

	uint8_t Ycmd[3] = {ICM20602_YA_OFFSET_H | (ICM20602_SPI_Write_Bit << 7), (uint8_t)(YOffset >> 8), (uint8_t)(YOffset)};
	SPIWrite(usr_SPI, Ycmd, sizeof(Ycmd));

	uint8_t Zcmd[3] = {ICM20602_ZA_OFFSET_H | (ICM20602_SPI_Write_Bit << 7), (uint8_t)(ZOffset >> 8), (uint8_t)(ZOffset)};
	SPIWrite(usr_SPI, Zcmd, sizeof(Zcmd));
}


uint8_t icm20602_getSmplrtDiv(usr_SPI_t *usr_SPI)
{
	uint8_t cmd = ICM20602_SMPLRT_DIV | (ICM20602_SPI_Read_Bit << 7);
	uint8_t data = 0;
	SPIBurstRead(usr_SPI, &cmd, &data, 1);

	return data;
}


void icm20602_setSmplrtDiv(usr_SPI_t *usr_SPI, uint8_t smplrtDiv)
{
	uint8_t cmd[] = {ICM20602_SMPLRT_DIV | (ICM20602_SPI_Write_Bit << 7), smplrtDiv};
	SPIWrite(usr_SPI, cmd, 2);
}


uint8_t icm20602_getGyroAccelFifoEN(usr_SPI_t *usr_SPI)
{
	uint8_t cmd = ICM20602_FIFO_EN | (ICM20602_SPI_Read_Bit << 7);
	uint8_t data = 0;
	SPIBurstRead(usr_SPI, &cmd, &data, 1);

	return data;
}


void icm20602_setGyroFifoEN(usr_SPI_t *usr_SPI, uint8_t enable)
{
	uint8_t data = icm20602_getGyroAccelFifoEN(usr_SPI);
	data &= 0b11101111;
	uint8_t cmd[] = {ICM20602_FIFO_EN | (ICM20602_SPI_Write_Bit << 7), data | (enable << 4)};
	SPIWrite(usr_SPI, cmd, 2);
}


void icm20602_setAccelFifoEN(usr_SPI_t *usr_SPI, uint8_t enable)
{
	uint8_t data = icm20602_getGyroAccelFifoEN(usr_SPI);
	data &= 0b11110111;
	uint8_t cmd[] = {ICM20602_FIFO_EN | (ICM20602_SPI_Write_Bit << 7), data | (enable << 3)};
	SPIWrite(usr_SPI, cmd, 2);
}


uint8_t icm20602_getIntConfig(usr_SPI_t *usr_SPI)
{
	uint8_t cmd = ICM20602_INT_PIN_CFG | (ICM20602_SPI_Read_Bit << 7);
	uint8_t data = 0;
	SPIBurstRead(usr_SPI, &cmd, &data, 1);

	return data;
}


void icm20602_setIntConfig(usr_SPI_t *usr_SPI, uint8_t config)
{
	uint8_t data = icm20602_getIntConfig(usr_SPI);
	data &= 0b00000011;
	uint8_t cmd[] = {ICM20602_INT_PIN_CFG | (ICM20602_SPI_Write_Bit << 7), data | config};
	SPIWrite(usr_SPI, cmd, 2);
}


uint8_t icm20602_getIntEnable(usr_SPI_t *usr_SPI)
{
	uint8_t cmd = ICM20602_INT_ENABLE | (ICM20602_SPI_Read_Bit << 7);
	uint8_t data = 0;
	SPIBurstRead(usr_SPI, &cmd, &data, 1);

	return data;
}


void icm20602_setIntEnable(usr_SPI_t *usr_SPI, uint8_t config)
{
	uint8_t data = icm20602_getIntEnable(usr_SPI);
	data &= 0b00000010;
	uint8_t cmd[2] = {ICM20602_INT_ENABLE | (ICM20602_SPI_Write_Bit << 7), data | config};
	SPIWrite(usr_SPI, cmd, 2);
}



uint8_t icm20602_getIntStatus(usr_SPI_t *usr_SPI)
{
	uint8_t cmd = ICM20602_INT_STATUS | (ICM20602_SPI_Read_Bit << 7);
	uint8_t data = 0;
	SPIBurstRead(usr_SPI, &cmd, &data, 1);

	return data;
}


int16_t icm20602_getXAccel(usr_SPI_t *usr_SPI)
{
	uint8_t cmd[] = {ICM20602_ACCEL_XOUT_H | (ICM20602_SPI_Read_Bit << 7), ICM20602_ACCEL_XOUT_L | (ICM20602_SPI_Read_Bit << 7)};
	uint8_t data[2];
	SPIRead(usr_SPI, cmd, data, 2);

	int16_t accel = ((data[0] << 8) | data[1]);

	return accel;
}


int16_t icm20602_getYAccel(usr_SPI_t *usr_SPI)
{
	uint8_t cmd[] = {ICM20602_ACCEL_YOUT_H | (ICM20602_SPI_Read_Bit << 7), ICM20602_ACCEL_YOUT_L | (ICM20602_SPI_Read_Bit << 7)};
	uint8_t data[2];
	SPIRead(usr_SPI, cmd, data, 2);

	int16_t accel = ((data[0] << 8) | data[1]);

	return accel;
}


int16_t icm20602_getZAccel(usr_SPI_t *usr_SPI)
{
	uint8_t cmd[] = {ICM20602_ACCEL_ZOUT_H | (ICM20602_SPI_Read_Bit << 7), ICM20602_ACCEL_ZOUT_L | (ICM20602_SPI_Read_Bit << 7)};
	uint8_t data[2];
	SPIRead(usr_SPI, cmd, data, 2);

	int16_t accel = ((data[0] << 8) | data[1]);

	return accel;
}


float icm20602_getTemperature(usr_SPI_t *usr_SPI)
{
	uint8_t cmd[] = {ICM20602_TEMP_OUT_H | (ICM20602_SPI_Read_Bit << 7), ICM20602_TEMP_OUT_L | (ICM20602_SPI_Read_Bit << 7)};
	uint8_t data[2];
	SPIRead(usr_SPI, cmd, data, 2);

	float temp = ((data[0] << 8) | data[1]);
	temp /= 326.8;
	temp += 25.0;

	return temp;
}


int16_t icm20602_getXGyro(usr_SPI_t *usr_SPI)
{
	uint8_t cmd[] = {ICM20602_GYRO_XOUT_H | (ICM20602_SPI_Read_Bit << 7), ICM20602_GYRO_XOUT_L | (ICM20602_SPI_Read_Bit << 7)};
	uint8_t data[2];
	SPIRead(usr_SPI, cmd, data, 2);

	int16_t gyro = ((data[0] << 8) | data[1]);

	return gyro;
}


int16_t icm20602_getYGyro(usr_SPI_t *usr_SPI)
{
	uint8_t cmd[] = {ICM20602_GYRO_YOUT_H | (ICM20602_SPI_Read_Bit << 7), ICM20602_GYRO_YOUT_L | (ICM20602_SPI_Read_Bit << 7)};
	uint8_t data[2];
	SPIRead(usr_SPI, cmd, data, 2);

	int16_t gyro = ((data[0] << 8) | data[1]);

	return gyro;
}


int16_t icm20602_getZGyro(usr_SPI_t *usr_SPI)
{
	uint8_t cmd[] = {ICM20602_GYRO_ZOUT_H | (ICM20602_SPI_Read_Bit << 7), ICM20602_GYRO_ZOUT_L | (ICM20602_SPI_Read_Bit << 7)};
	uint8_t data[2];
	SPIRead(usr_SPI, cmd, data, 2);

	int16_t gyro = ((data[0] << 8) | data[1]);

	return gyro;
}


uint16_t icm20602_getFifoWatermark(usr_SPI_t *usr_SPI)
{
	uint8_t cmd[] = {ICM20602_FIFO_WM_TH1 | (ICM20602_SPI_Read_Bit << 7), ICM20602_FIFO_WM_TH2 | (ICM20602_SPI_Read_Bit << 7)};
	uint8_t data[2];
	SPIRead(usr_SPI, cmd, data, 2);

	return (uint16_t)(((data[0] & 0b00000011) << 8) | data[1]);
}


void icm20602_setFifoWatermark(usr_SPI_t *usr_SPI, uint16_t wm_level)
{
	uint8_t cmd[] = {ICM20602_FIFO_WM_TH1 | (ICM20602_SPI_Write_Bit << 7), (uint8_t)((wm_level >> 8) & 0b00000011),
			ICM20602_FIFO_WM_TH2 | (ICM20602_SPI_Write_Bit << 7), (uint8_t)wm_level};
	SPIWrite(usr_SPI, cmd, sizeof(cmd));
}


uint8_t icm20602_getFifoEN(usr_SPI_t *usr_SPI)
{
	uint8_t cmd = ICM20602_USER_CTRL | (ICM20602_SPI_Read_Bit << 7);
	uint8_t data = 0;
	SPIBurstRead(usr_SPI, &cmd, &data, 1);

	return data & 0b01000000;
}


void icm20602_setFifoEN(usr_SPI_t *usr_SPI, uint8_t enable)
{
	uint8_t data = icm20602_getFifoEN(usr_SPI);
	data &= 0b10111111;
	uint8_t cmd[] = {ICM20602_USER_CTRL | (ICM20602_SPI_Write_Bit << 7), data | (enable << 6)};
	SPIWrite(usr_SPI, cmd, 2);
}


void icm20602_getFifo(usr_SPI_t *usr_SPI, uint8_t *data, uint16_t dataLen)
{
	uint8_t cmd = ICM20602_FIFO_R_W | (ICM20602_SPI_Read_Bit << 7);
	SPIBurstRead(usr_SPI, &cmd, data, dataLen);
}


/**
 * Reads data from Fifo and assembles low and high values of the respective
 * fifo entries to 16 bit integer values
 *
 * @param usr_SPI pointer to the HAL spi instance to use
 * @param data pointer to where the data will be stored
 * @param dataLen how many bytes shall be read from fifo
 * IMPORTANT: dataLen must be a multiple of 2 because fifo
 * should always contain one HIGH and one LOW data byte of any sensor!
 */
void icm20602_getFifoFused(usr_SPI_t *usr_SPI, int16_t *data, uint16_t dataLen)
{
	uint8_t temp[dataLen];
	icm20602_getFifo(usr_SPI, temp, dataLen);
	for(uint16_t i = 0; i < dataLen; i += 2)
	{
		data[i/2] = (int16_t)((temp[i] << 8) | temp[i+1]);
	}
}

//TODO: improve function
void icm20602_getFifoMotion6(usr_SPI_t *usr_SPI, int16_t *data, uint16_t dataLen)	// returns GyroX, GyroY, GyroZ, AccelX, AccelY, AccelZ in data buffer in this order
{
	if(dataLen >= 6)
	{
		uint8_t tmp[14];
		icm20602_getFifo(usr_SPI, tmp, 14);

		data[0] = (int16_t)((tmp[8] << 8) | tmp[9]);
		data[1] = (int16_t)((tmp[10] << 8) | tmp[11]);
		data[2] = (int16_t)((tmp[12] << 8) | tmp[13]);
		data[3] = (int16_t)((tmp[0] << 8) | tmp[1]);
		data[4] = (int16_t)((tmp[2] << 8) | tmp[3]);
		data[5] = (int16_t)((tmp[4] << 8) | tmp[5]);
	}
}

/**
 * @brief converts raw gyro representation to �/s (degrees per second)
 * @param usr_SPI pointer to the HAL spi instance to use
 * @param raw the raw integer representation of the gyro rate
 * @retval the converted value as a float
 */
double icm20602_raw2DegPerSec(usr_SPI_t *usr_SPI, int16_t raw)
{
	double retval = 0.f;

	switch(icm20602_gyroFS)
	{
	case ICM20602_GYRO_FS_250:
		retval = map_float((double)raw, -32768., 32767., -251., 250.);
		break;
	case ICM20602_GYRO_FS_500:
		retval = map_float((double)raw, -32768., 32767., -501., 500.);
		break;
	case ICM20602_GYRO_FS_1000:
		retval = map_float((double)raw, -32768., 32767., -1001., 1000.);
		break;
	case ICM20602_GYRO_FS_2000:
		retval = map_float((double)raw, -32768., 32767., -2001., 2000.);
		break;
	}

	return retval;
}
