/* USER CODE BEGIN Header */
/**
 ******************************************************************************
 * @file           : main.c
 * @brief          : Main program body
 ******************************************************************************
 * @attention
 *
 * <h2><center>&copy; Copyright (c) 2020 STMicroelectronics.
 * All rights reserved.</center></h2>
 *
 * This software component is licensed by ST under BSD 3-Clause license,
 * the "License"; You may not use this file except in compliance with the
 * License. You may obtain a copy of the License at:
 *                        opensource.org/licenses/BSD-3-Clause
 *
 ******************************************************************************
 */
/* USER CODE END Header */

/* Includes ------------------------------------------------------------------*/
#include "main.h"
#include "dma.h"
#include "spi.h"
#include "usart.h"
#include "usb_device.h"
#include "gpio.h"

/* Private includes ----------------------------------------------------------*/
/* USER CODE BEGIN Includes */
//#include "systick_extension.h" cannot be used parallel to HAL
#include "print_usb.h"
#include "icm20602.h"
#include <string.h>
#include "leds.h"
#include "sbus.h"
#include "useful_functions.h"
#include "dshot.h"
#include "timers_custom.h"
#include "buzzer.h"
#include "bat_measure.h"
#include "pid_controller.h"
#include "rates_and_gains.h"
#include "filters.h"
#include "motors.h"
#include "escTlm.h"
#include <stdio.h>
#include "retarget.h"

/* USER CODE END Includes */

/* Private typedef -----------------------------------------------------------*/
/* USER CODE BEGIN PTD */

/* USER CODE END PTD */

/* Private define ------------------------------------------------------------*/
/* USER CODE BEGIN PD */
//#define PID_TUNING_ENABLE	// uncomment to enable pid tuning
#define WAIT_FOR_USB_CONNECTION	// uncomment --> fc will initialize and wait for newline character
/* USER CODE END PD */

/* Private macro -------------------------------------------------------------*/
/* USER CODE BEGIN PM */

/* USER CODE END PM */

/* Private variables ---------------------------------------------------------*/

/* USER CODE BEGIN PV */
uint32_t now;

PIDController_t yaw_PID;
PIDController_t pitch_PID;
PIDController_t roll_PID;

usr_SPI_t AccelGyro;

SBUS_t receiver;
/* USER CODE END PV */

/* Private function prototypes -----------------------------------------------*/
void SystemClock_Config(void);
/* USER CODE BEGIN PFP */

/* USER CODE END PFP */

/* Private user code ---------------------------------------------------------*/
/* USER CODE BEGIN 0 */

/* USER CODE END 0 */

/**
  * @brief  The application entry point.
  * @retval int
  */
int main(void)
{
  /* USER CODE BEGIN 1 */

  /* USER CODE END 1 */

  /* MCU Configuration--------------------------------------------------------*/

  /* Reset of all peripherals, Initializes the Flash interface and the Systick. */
  HAL_Init();

  /* USER CODE BEGIN Init */

  /* USER CODE END Init */

  /* Configure the system clock */
  SystemClock_Config();

  /* USER CODE BEGIN SysInit */

  /* USER CODE END SysInit */

  /* Initialize all configured peripherals */
  MX_GPIO_Init();
  MX_DMA_Init();
  MX_USB_DEVICE_Init();
  MX_SPI2_Init();
  MX_UART5_Init();
  /* USER CODE BEGIN 2 */
  	  extern USBD_HandleTypeDef hUsbDeviceFS;

	CUSTOM_DMA_Init();
	TIM2_Init();
	TIM3_Init();
	TIM5_Init();	// call this only AFTER dma init


	/* PRINT OVER USB SETUP */
	printUsb_init();
	RetargetInit();
	/* PRINT OVER USB SETUP END*/


	/* SBUS SETUP */
	extern UART_HandleTypeDef huart5;	// file "usart.c"
	receiver = sbus_begin(&huart5);	// start uart5 dma
	/* SBUS SETUP END */


	/* LIPO MEASURMENT SETUP */
	bat_measure_init();
	/* LIPO MEASURMENT SETUP END */


	/* AccelGyro SETUP */
	extern SPI_HandleTypeDef hspi2;	// file "spi.c"
	AccelGyro = usr_SPI_init(&hspi2, Gyro_NSS_GPIO_Port, Gyro_NSS_Pin, 100);
	icm20602_reset(&AccelGyro);
	//HAL_Delay(1000);

	toggleRed();
	while(!icm20602_testConnection(&AccelGyro))
	{
		toggleRed();
		toggleOrange();
		HAL_Delay(100);
	}
	toggleRed();

	icm20602_init(&AccelGyro);
	/* enable fifo watermark interrupt and set the watermark level
	 * to:
	 * - 4*2 bytes if gyro (3 values) & temp (1 value) ...
	 * - 4*2 bytes if accel (3 values) & temp (1 value) ...
	 * - 7*2 bytes if gyro (3 values) & accel (3 values) & temp (1 value) ...
	 * ... fifos are enabled
	 * NOTE:
	 * temp fifo is enabled by default
	 * it is possible to disable temp fifo --> minus 1 value <=> minus 2 bytes
	 */
	icm20602_setGyroFS(&AccelGyro, ICM20602_GYRO_FS_1000);
	//icm20602_setGyroOffset(&AccelGyro, -27, 23, 6);
	/*
	 * X		Y		Z
	 * -21.15	15.81	2.94
	 * -20.95	15.70	2.89
	 * -20.87	15.66	2.89
	 * -20.88	15.65	2.90
	 */
	uint16_t gyro_offset_XYZ[] = {17, -9, -3};
#define GYRO_RAW_CUTOFF	10	// raw gyro values will be set to zero if -GYRO_RAW_CUTOFF < |gyro_val| < GYRO_RAW_CUTOFF
#define PRINT_GYRO_CONTINUOUS
#define GYRO_PRINT_PERIOD_MS	5
//#define PRINT_GYRO_AVG
	icm20602_setFifoWatermark(&AccelGyro, 4*2);	// watermark for gyro and temp fifos enabled
	icm20602_setFifoEN(&AccelGyro, 1);	// enable fifo
	icm20602_setGyroFifoEN(&AccelGyro, 1);	// enable gyro fifo
	/* AccelGyro SETUP END */


	/* PID CONTROLLER SETUP */
	// must be after lipo measurement init!
	float PIDScale = bat_measure_getPIDScalar();
	float Kp_roll = KP_ROLL_3S * PIDScale;
	float Ki_roll = KI_ROLL_3S * PIDScale;
	float Kd_roll = KD_ROLL_3S * PIDScale;
	float Kp_pitch = KP_PITCH_3S * PIDScale;
	float Ki_pitch = KI_PITCH_3S * PIDScale;
	float Kd_pitch = KD_PITCH_3S * PIDScale;
	float Kp_yaw = KP_YAW_3S * PIDScale;
	float Ki_yaw = KI_YAW_3S * PIDScale;
	float Kd_yaw = KD_YAW_3S * PIDScale;
	pid_init(&yaw_PID, Kp_yaw, Ki_yaw, Kd_yaw, TAU_YAW, PID_RANGE_MIN, PID_RANGE_MAX);
	pid_init(&pitch_PID, Kp_pitch, Ki_pitch, Kd_pitch, TAU_PITCH, PID_RANGE_MIN, PID_RANGE_MAX);
	pid_init(&roll_PID, Kp_roll, Ki_roll, Kd_roll, TAU_ROLL, PID_RANGE_MIN, PID_RANGE_MAX);
	/* PID CONTROLLER SETUP END */


	/* FILTER SETUP */
	filter_t *gyro_filter_XYZ[3];
	gyro_filter_XYZ[0] = filter_init(iir_lpf_o10_Fs1000_Fc100);
	gyro_filter_XYZ[1] = filter_init(iir_lpf_o10_Fs1000_Fc100);
	gyro_filter_XYZ[2] = filter_init(iir_lpf_o10_Fs1000_Fc80);
	/* FILTER SSETUP END */


	/* MOTOR SETUP */
	motor_t m_bl;
	m_bl.position = back_left;
	m_bl.direction = cw;
	motor_t m_br;
	m_br.position = back_right;
	m_br.direction = ccw;
	motor_t m_fr;
	m_fr.position = front_right;
	m_fr.direction = cw;
	motor_t m_fl;
	m_fl.position = front_left;
	m_fl.direction = ccw;
	/* MOTOR SETUP END */


	/* DSHOT SETUP */
	dshot_init();
	/* DSHOT SETUP END */


	/* ESC TELEMETRY SETUP */
	// wait for usb connection
	/*while(hUsbDeviceFS.dev_state != USBD_STATE_CONFIGURED)
	{
		blinkOrange(500,500);
		blinkRed(100,100);
	}*/
	redOn();
	uint32_t n = HAL_GetTick();
	while(HAL_GetTick() < n + 4000){};
	printf("[INFO] USB CONNECTED\n");
	usbSend();
	redOff();
	orangeOn();
	n = HAL_GetTick();
	while(HAL_GetTick() < n + 1000){};
	escTlm_init();
	printf("[INFO] ESC TELEMETRY INITIAZLIZED\n");
	usbSend();
	// check if everything works
	uint16_t m[] = {0,0,0,0};
	for(int ii = 0; ii < 10; ii++)
	{
		HAL_Delay(1);
		uint8_t tlm[] = {0,0,0,0};
		dshot_generateCmd(m, tlm, 4);
		dshot_sendAll();
	}
	orangeOff();
	printf("[INFO] ESCs INITIAZLIZED\n");
	usbSend();
	for(int i = 0; i < 4; i++)
	{
		HAL_Delay(1);
		uint8_t tlm_rqst[] = {0,0,0,0};
		tlm_rqst[i] = 1;
		dshot_generateCmd(m, tlm_rqst, 4);
		dshot_sendAll();
		HAL_Delay(1);
		uint8_t uart_rdr_content = UART4->RDR;
		if(uart_rdr_content == 0)
		{
			redOn();
			orangeOff();
		}
		else
		{
			redOff();
			orangeOn();
		}
		while(1);	// infinite loop
		uint8_t tlm_buf[ESCTLM_PACKET_SIZE];
		uint8_t tempvar = escTlm_read((uint8_t *)&tlm_buf);
		while(tempvar != 0){
			if(tempvar == 1)
			{
				printf("[WARNING] Framing error\n");
			}
			else if(tempvar == 2)
			{
				printf("[INFO] ESC TLM no new data. Status Nr: %u\n", escTlm_current_status);
			}
			usbSend();
			blinkOrange(100,500);
			HAL_Delay(100);
			tempvar = escTlm_read((uint8_t *)&tlm_buf);
		}
		orangeOff();
		if(escTlm_crc_valid((uint8_t *)&tlm_buf))
		{
			printStr("[INFO] CRC OK");
		}
		else
		{
			printStr("[WARNING] CRC BAD");
		}
		printLn();
		usbSend();
	}

	/* ESC TELEMETRY SETUP END */

  /* USER CODE END 2 */

  /* Infinite loop */
  /* USER CODE BEGIN WHILE */
	HAL_Delay(500);
	orangeOff();
	HAL_Delay(500);

	now = HAL_GetTick();

	uint8_t armed = 0;
	uint8_t firstArm = 1;
	float RC_Cmd_YPRT[] = {0.0f, 0.0f, 0.0f, 0.0f};	// yaw, pitch, roll, throttle RC command
													// in ??/s (degrees per second)

#ifdef PRINT_GYRO_AVG
	double gyro_XYZ_avg[] = {0.0f, 0.0f, 0.0f};
	uint32_t gyro_avg_counter = 0;
#endif

	while (1)
	{
		/* ----------            RC UPDATE            ---------- */
		if(sbus_updateFlag)
		{
			sbus_clearUpdateFlag();
			sbus_getChannels(&receiver);
			if(receiver.channels[4] < SBUS_CHANNEL_MID)
			{
				armed = 0;
				firstArm = 1;

				// reset Yaw, Pitch, Roll, Throttle command values
				RC_Cmd_YPRT[0] = 0.0f;
				RC_Cmd_YPRT[1] = 0.0f;
				RC_Cmd_YPRT[2] = 0.0f;
				RC_Cmd_YPRT[3] = 0.0f;
			}
			else
			{
				armed = 1;

				// extract Yaw, Pitch, Roll, Throttle command values
				// Yaw
				RC_Cmd_YPRT[0] = (float)(map((int16_t)(receiver.channels[SBUS_YAW_CHANNELINDEX]),
												SBUS_CHANNEL_MIN, SBUS_CHANNEL_MAX,
												RC_YAW_RATE_MIN, RC_YAW_RATE_MAX));
				// Pitch
				RC_Cmd_YPRT[1] = (float)(map((int16_t)(receiver.channels[SBUS_PITCH_CHANNELINDEX]),
												SBUS_CHANNEL_MIN, SBUS_CHANNEL_MAX,
												RC_RATE_MIN, RC_RATE_MAX));
				// Roll
				RC_Cmd_YPRT[2] = (float)(map((int16_t)(receiver.channels[SBUS_ROLL_CHANNELINDEX]),
												SBUS_CHANNEL_MIN, SBUS_CHANNEL_MAX,
												RC_RATE_MIN, RC_RATE_MAX));
				//Throttle
				RC_Cmd_YPRT[3] = (float)(map((int16_t)(receiver.channels[SBUS_THROTTLE_CHANNELINDEX]),
												SBUS_CHANNEL_MIN, SBUS_CHANNEL_MAX,
												DSHOT_MOTOR_MIN, DSHOT_MOTOR_MAX - PID_RANGE_MAX));	// leave space for control
			}

			// if throttle is not at minimum don't arm!
			// if safety switch is not held simultaneously to arming switch don't arm!
			if(armed == 1 && firstArm == 1 && ( (uint16_t)RC_Cmd_YPRT[3] > DSHOT_MOTOR_MIN || receiver.channels[SBUS_MOMENTARY_SWITCH_CHANNELINDEX] < SBUS_CHANNEL_MAX) )
			{
				armed = 0;

				// reset Yaw, Pitch, Roll, Throttle command values
				RC_Cmd_YPRT[0] = 0.0f;
				RC_Cmd_YPRT[1] = 0.0f;
				RC_Cmd_YPRT[2] = 0.0f;
				RC_Cmd_YPRT[3] = 0.0f;
			}
			else if(armed == 1 && firstArm == 1 && (uint16_t)RC_Cmd_YPRT[3] <= DSHOT_MOTOR_MIN && receiver.channels[SBUS_MOMENTARY_SWITCH_CHANNELINDEX] >= SBUS_CHANNEL_MAX)
			{
				firstArm = 0;
			}

			/*
			 * PID-tuning
			 */
#ifdef PID_TUNING_ENABLE
			if(receiver.channels[5] == SBUS_CHANNEL_MID)	// enable PITCH/ROLL PID tuning mode
			{
				if(receiver.channels[6] == SBUS_CHANNEL_MIN)	// kp-tuning
				{
					Kp = constrain_float(
							map_float(
									(float)receiver.channels[7],
									(float)SBUS_CHANNEL_MIN,
									(float)SBUS_CHANNEL_MAX,
									0.f,
									2.f),
							0.f,
							2.f);
					pid_setPGain(&pitch_PID, Kp);
					pid_setPGain(&roll_PID, Kp);

				}
				else if(receiver.channels[6] == SBUS_CHANNEL_MID)	// ki-tuning
				{
					Ki = constrain_float(
							map_float(
									(float)receiver.channels[7],
									(float)SBUS_CHANNEL_MIN,
									(float)SBUS_CHANNEL_MAX,
									0.f,
									2.f),
							0.f,
							2.f);
					pid_setPGain(&pitch_PID, Ki);
					pid_setPGain(&roll_PID, Ki);
				}
				else if(receiver.channels[6] == SBUS_CHANNEL_MAX)	// kd-tuning
				{
					Kd = constrain_float(
							map_float(
									(float)receiver.channels[7],
									(float)SBUS_CHANNEL_MIN,
									(float)SBUS_CHANNEL_MAX,
									0.f,
									2.f),
							0.f,
							2.f);
					pid_setPGain(&pitch_PID, Kd);
					pid_setPGain(&roll_PID, Kd);
				}
				printFloat(Kp, 2);
				printTab();
				printFloat(Ki, 2);
				printTab();
				printFloat(Kd, 2);
				printLn();
			}
			else if(receiver.channels[5] == SBUS_CHANNEL_MAX)	// enable YAW PID tuning mode
			{
				if(receiver.channels[6] == SBUS_CHANNEL_MIN)	// kp-tuning
				{
					Kp_yaw = constrain_float(
							map_float(
									(float)receiver.channels[7],
									(float)SBUS_CHANNEL_MIN,
									(float)SBUS_CHANNEL_MAX,
									0.f,
									4.f),
							0.f,
							4.f);
					pid_setPGain(&yaw_PID, Kp_yaw);
				}
				else if(receiver.channels[6] == SBUS_CHANNEL_MID)	// ki-tuning
				{
					Ki_yaw = constrain_float(
							map_float(
									(float)receiver.channels[7],
									(float)SBUS_CHANNEL_MIN,
									(float)SBUS_CHANNEL_MAX,
									0.f,
									4.f),
							0.f,
							4.f);
					pid_setPGain(&yaw_PID, Ki_yaw);
				}
				else if(receiver.channels[6] == SBUS_CHANNEL_MAX)	// kd-tuning
				{
					Kd_yaw = constrain_float(
							map_float(
									(float)receiver.channels[7],
									(float)SBUS_CHANNEL_MIN,
									(float)SBUS_CHANNEL_MAX,
									0.f,
									4.f),
							0.f,
							4.f);
					pid_setPGain(&yaw_PID, Kd_yaw);
				}
				printFloat(Kp_yaw, 2);
				printTab();
				printFloat(Ki_yaw, 2);
				printTab();
				printFloat(Kd_yaw, 2);
				printLn();
			}
#endif	// PID_TUNING_ENABLE
		}
		/* ----------          RC UPDATE END          ---------- */


		/* ==========          UPDATE CYCLE           ========== */
		if(icm20602_intFlag)
		{
			int16_t Gyro_data[4];	// raw gyro values: Temperature, Roll, Pitch, Yaw
			float gyro_YPR[3] = {0.0f, 0.0f, 0.0f};	// gyro Yaw, Pitch, Roll measured rates
													// in °/s (degrees per second)
			float PID_values_YPR[] = {0.0f, 0.0f, 0.0f};	// PID Yaw, Pitch, Roll calculated values

			/* MOTOR MAPPING:
			 * motor 1 <-> back		left	ccw
			 * motor 2 <-> back		right	cw
			 * motor 3 <-> front	right	ccw
			 * motor 4 <-> front	left	cw
			 */
			uint16_t motor_values[] = {0, 0, 0, 0};	// throttle commands for motors 1, 2, 3 and 4 i this order
			uint8_t tlm_rqst[4] = {0,0,0,0};

		/* ----------           GYRO UPDATE           ---------- */
			icm20602_clearIntFlag();
			icm20602_getFifoFused(&AccelGyro, Gyro_data, 8);
			// add gyro offsets
			Gyro_data[1] += gyro_offset_XYZ[0];
			Gyro_data[2] += gyro_offset_XYZ[1];
			Gyro_data[3] += gyro_offset_XYZ[2];

		/* ----------           GYRO FILTER           ---------- */
			Gyro_data[1] = iir_evaluate(gyro_filter_XYZ[0], Gyro_data[1]);
			Gyro_data[2] = iir_evaluate(gyro_filter_XYZ[1], Gyro_data[2]);
			Gyro_data[3] = iir_evaluate(gyro_filter_XYZ[2], Gyro_data[3]);

			/*if(Gyro_data[1] < GYRO_RAW_CUTOFF && Gyro_data[1] > -GYRO_RAW_CUTOFF)
			{
				Gyro_data[1] = 0;
			}
			if(Gyro_data[2] < GYRO_RAW_CUTOFF && Gyro_data[2] > -GYRO_RAW_CUTOFF)
			{
				Gyro_data[2] = 0;
			}
			if(Gyro_data[3] < GYRO_RAW_CUTOFF && Gyro_data[3] > -GYRO_RAW_CUTOFF)
			{
				Gyro_data[3] = 0;
			}*/

#ifdef PRINT_GYRO_CONTINUOUS
			if(update(GYRO_PRINT_PERIOD_MS) == 1)
			{
				printf("%d,%d,%d\n", Gyro_data[1], Gyro_data[2], Gyro_data[3]);
			}
#endif

#ifdef PRINT_GYRO_AVG
			if(gyro_avg_counter < 100000)
			{
				gyro_XYZ_avg[0] += (double)(Gyro_data[1]);
				gyro_XYZ_avg[1] += (double)(Gyro_data[2]);
				gyro_XYZ_avg[2] += (double)(Gyro_data[3]);
				gyro_avg_counter ++;
				if(gyro_avg_counter % 1000 == 0)
				{
					printNum(gyro_avg_counter/1000);
					printStr("%\n");
					usbSend();
				}
			}
			else
			{
				gyro_XYZ_avg[0] /= (double)(gyro_avg_counter);
				gyro_XYZ_avg[1] /= (double)(gyro_avg_counter);
				gyro_XYZ_avg[2] /= (double)(gyro_avg_counter);
				printFloat(gyro_XYZ_avg[0], 2);
				printTab();
				printFloat(gyro_XYZ_avg[1], 2);
				printTab();
				printFloat(gyro_XYZ_avg[2], 2);
				printLn();
				usbSend();
				gyro_avg_counter = 0;
			}
#endif	// PRINT_GYRO_AVG

			// Yaw
			gyro_YPR[0] = (-1.f) * icm20602_raw2DegPerSec(&AccelGyro, Gyro_data[3]);
			// Pitch
			gyro_YPR[1] = (-1.f) * icm20602_raw2DegPerSec(&AccelGyro, Gyro_data[2]);
			// Roll
			gyro_YPR[2] = (-1.f) * icm20602_raw2DegPerSec(&AccelGyro, Gyro_data[1]);


		/* ----------         GYRO UPDATE END         ---------- */


		/* ----------           PID UPDATE            ---------- */
			if(armed == 1 && sbus_isFailsaveActive(&receiver) == SBUS_FAILSAFE_INACTIVE)
			{
				// Update PID loop
				// Throttle Ki adjust
				pid_throttleIGainAdjust(&yaw_PID, RC_Cmd_YPRT[3], 0, DSHOT_MOTOR_MAX);
				pid_throttleIGainAdjust(&pitch_PID, RC_Cmd_YPRT[3], 0, DSHOT_MOTOR_MAX);
				pid_throttleIGainAdjust(&roll_PID, RC_Cmd_YPRT[3], 0, DSHOT_MOTOR_MAX);
				// Yaw
				PID_values_YPR[0] = pid_update(&yaw_PID, RC_Cmd_YPRT[0], gyro_YPR[0]);
				// Pitch
				PID_values_YPR[1] = pid_update(&pitch_PID, RC_Cmd_YPRT[1], gyro_YPR[1]);
				// Roll
				PID_values_YPR[2] = pid_update(&roll_PID, RC_Cmd_YPRT[2], gyro_YPR[2]);
			}
			else
			{
				pid_reset(&yaw_PID);
				pid_reset(&pitch_PID);
				pid_reset(&roll_PID);
			}

			/*for(int i = 0; i<3; i++)
			{
				printFloat(PID_values_YPR[i],2);
				printTab();
			}
			printLn();*/

		/* ----------         PID UPDATE END          ---------- */


		/* ----------          MOTOR UPDATE           ---------- */
			if(armed == 1 && sbus_isFailsaveActive(&receiver) == SBUS_FAILSAFE_INACTIVE)
			{
				/*
				 * Compute individual throttle values for motors
				 */
				// Back Left motor, cw
				motor_values[m_bl.position] = (uint16_t)(constrain((int32_t)(RC_Cmd_YPRT[3] - PID_values_YPR[0] + PID_values_YPR[1] + PID_values_YPR[2]), DSHOT_MOTOR_MIN, DSHOT_MOTOR_MAX));
				// Back Right motor, ccw
				motor_values[m_br.position] = (uint16_t)(constrain((int32_t)(RC_Cmd_YPRT[3] + PID_values_YPR[0] + PID_values_YPR[1] - PID_values_YPR[2]), DSHOT_MOTOR_MIN, DSHOT_MOTOR_MAX));
				// Front Right motor, cw
				motor_values[m_fr.position] = (uint16_t)(constrain((int32_t)(RC_Cmd_YPRT[3] - PID_values_YPR[0] - PID_values_YPR[1] - PID_values_YPR[2]), DSHOT_MOTOR_MIN, DSHOT_MOTOR_MAX));
				// Front Left motor, ccw
				motor_values[m_fl.position] = (uint16_t)(constrain((int32_t)(RC_Cmd_YPRT[3] + PID_values_YPR[0] - PID_values_YPR[1] + PID_values_YPR[2]), DSHOT_MOTOR_MIN, DSHOT_MOTOR_MAX));
			}

			if(!dshot_isBusy())
			{
				dshot_generateCmd(motor_values, tlm_rqst, 4);
				dshot_sendAll();
			}
		/* ----------        MOTOR UPDATE END         ---------- */


		/* ----------           REST UPDATE           ---------- */
			float vbat = bat_measure_getBatVoltage();

		// ----- LEDS -----
			if(
					receiver.channels[SBUS_MOMENTARY_SWITCH_CHANNELINDEX] >= SBUS_CHANNEL_MAX
					|| sbus_isFailsaveActive(&receiver) == SBUS_FAILSAFE_ACTIVE
					)
			{
				blinkRed(100, 100);
			}
			else if(armed)
			{
				blinkRed(50, 450);
			}
			else
			{
				redOff();
			}

			// orange led as status led --> when orange off --> You're in TROUBLE...
			blinkOrange(100, 900);

		// ----- BUZZER -----
			if(
					vbat < lipo_vcrit
					|| receiver.channels[SBUS_MOMENTARY_SWITCH_CHANNELINDEX] >= SBUS_CHANNEL_MAX
					|| sbus_isFailsaveActive(&receiver) == SBUS_FAILSAFE_ACTIVE
					)
			{
				buzzerOn();
			}
			else
			{
				buzzerOff();
			}

			bat_measure_start();	// start new battery measurement cycle
		/* ----------         REST UPDATE END         ---------- */

		}
		/* ==========        UPDATE CYCLE END         ========== */


    /* USER CODE END WHILE */

    /* USER CODE BEGIN 3 */
	}
  /* USER CODE END 3 */
}

/**
  * @brief System Clock Configuration
  * @retval None
  */
void SystemClock_Config(void)
{
  RCC_OscInitTypeDef RCC_OscInitStruct = {0};
  RCC_ClkInitTypeDef RCC_ClkInitStruct = {0};
  RCC_PeriphCLKInitTypeDef PeriphClkInitStruct = {0};

  /** Configure the main internal regulator output voltage 
  */
  __HAL_RCC_PWR_CLK_ENABLE();
  __HAL_PWR_VOLTAGESCALING_CONFIG(PWR_REGULATOR_VOLTAGE_SCALE1);
  /** Initializes the CPU, AHB and APB busses clocks 
  */
  RCC_OscInitStruct.OscillatorType = RCC_OSCILLATORTYPE_HSE;
  RCC_OscInitStruct.HSEState = RCC_HSE_ON;
  RCC_OscInitStruct.PLL.PLLState = RCC_PLL_ON;
  RCC_OscInitStruct.PLL.PLLSource = RCC_PLLSOURCE_HSE;
  RCC_OscInitStruct.PLL.PLLM = 4;
  RCC_OscInitStruct.PLL.PLLN = 216;
  RCC_OscInitStruct.PLL.PLLP = RCC_PLLP_DIV2;
  RCC_OscInitStruct.PLL.PLLQ = 9;
  if (HAL_RCC_OscConfig(&RCC_OscInitStruct) != HAL_OK)
  {
    Error_Handler();
  }
  /** Activate the Over-Drive mode 
  */
  if (HAL_PWREx_EnableOverDrive() != HAL_OK)
  {
    Error_Handler();
  }
  /** Initializes the CPU, AHB and APB busses clocks 
  */
  RCC_ClkInitStruct.ClockType = RCC_CLOCKTYPE_HCLK|RCC_CLOCKTYPE_SYSCLK
                              |RCC_CLOCKTYPE_PCLK1|RCC_CLOCKTYPE_PCLK2;
  RCC_ClkInitStruct.SYSCLKSource = RCC_SYSCLKSOURCE_PLLCLK;
  RCC_ClkInitStruct.AHBCLKDivider = RCC_SYSCLK_DIV1;
  RCC_ClkInitStruct.APB1CLKDivider = RCC_HCLK_DIV4;
  RCC_ClkInitStruct.APB2CLKDivider = RCC_HCLK_DIV2;

  if (HAL_RCC_ClockConfig(&RCC_ClkInitStruct, FLASH_LATENCY_7) != HAL_OK)
  {
    Error_Handler();
  }
  PeriphClkInitStruct.PeriphClockSelection = RCC_PERIPHCLK_UART5|RCC_PERIPHCLK_CLK48;
  PeriphClkInitStruct.Uart5ClockSelection = RCC_UART5CLKSOURCE_PCLK1;
  PeriphClkInitStruct.Clk48ClockSelection = RCC_CLK48SOURCE_PLL;
  if (HAL_RCCEx_PeriphCLKConfig(&PeriphClkInitStruct) != HAL_OK)
  {
    Error_Handler();
  }
}

/* USER CODE BEGIN 4 */
/* USER CODE END 4 */

/**
  * @brief  This function is executed in case of error occurrence.
  * @retval None
  */
void Error_Handler(void)
{
  /* USER CODE BEGIN Error_Handler_Debug */
	/* User can add his own implementation to report the HAL error return state */

  /* USER CODE END Error_Handler_Debug */
}

#ifdef  USE_FULL_ASSERT
/**
  * @brief  Reports the name of the source file and the source line number
  *         where the assert_param error has occurred.
  * @param  file: pointer to the source file name
  * @param  line: assert_param error line source number
  * @retval None
  */
void assert_failed(uint8_t *file, uint32_t line)
{ 
  /* USER CODE BEGIN 6 */
	/* User can add his own implementation to report the file name and line number,
     tex: printf("Wrong parameters value: file %s on line %d\r\n", file, line) */
  /* USER CODE END 6 */
}
#endif /* USE_FULL_ASSERT */

/************************ (C) COPYRIGHT STMicroelectronics *****END OF FILE****/
