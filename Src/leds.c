/*
 * leds.c
 *
 *  Created on: 15.02.2020
 *      Author: IchWerSonst
 */

#include <leds.h>

/**
 * @brief turns on red led
 * @param NONE
 * @retval NONE
 */
void redOn(void)
{
	GPIOC->BSRR |= GPIO_BSRR_BR_14;
}

/**
 * @brief turns off red led
 * @param NONE
 * @retval NONE
 */
void redOff(void)
{
	GPIOC->BSRR |= GPIO_BSRR_BS_14;
}

/**
 * @brief turns on orange led
 * @param NONE
 * @retval NONE
 */
void orangeOn(void)
{
	GPIOC->BSRR |= GPIO_BSRR_BR_13;
}

/**
 * @brief turns off orange led
 * @param NONE
 * @retval NONE
 */
void orangeOff(void)
{
	GPIOC->BSRR |= GPIO_BSRR_BS_13;
}

/**
 * @brief blinks red led with specified off duration and on duration
 * @param on_ms on duration in milliseconds
 * @param off_ms off duration in milliseconds
 * @retval NONE
 */
void blinkRed(uint16_t on_ms, uint16_t off_ms)
{
	static uint32_t last_time;

#ifndef HAL_MODULE_ENABLED
	uint32_t now_us = micros();
	if(now_us > last_time + (on_ms + off_ms) * 1000)
	{
		redOn();
		last_time = now_us;
	}
	else if(now_us > last_time + (on_ms * 1000))
	{
		redOff();
	}
#else
	uint32_t now_ms = HAL_GetTick();
	if(now_ms > last_time + on_ms + off_ms)
	{
		redOn();
		last_time = now_ms;
	}
	else if(now_ms > last_time + on_ms)
	{
		redOff();
	}
#endif
}

/**
 * @brief blinks orange led with specified off duration and on duration
 * @param on_ms on duration in milliseconds
 * @param off_ms off duration in milliseconds
 * @retval NONE
 */
void blinkOrange(uint16_t on_ms, uint16_t off_ms)
{
	static uint32_t last_time;

#ifndef HAL_MODULE_ENABLED
	uint32_t now_us = micros();
	if(now_us > last_time + (on_ms + off_ms) * 1000)
	{
		orangeOn();
		last_time = now_us;
	}
	else if(now_us > last_time + (on_ms * 1000))
	{
		orangeOff();
	}
#else
	uint32_t now_ms = HAL_GetTick();
	if(now_ms > last_time + on_ms + off_ms)
	{
		orangeOn();
		last_time = now_ms;
	}
	else if(now_ms > last_time + on_ms)
	{
		orangeOff();
	}
#endif
}

/**
 * toggles the led
 * @param NONE
 * @retval NONE
 */
void toggleRed(void)
{
	if(GPIOC->IDR & GPIO_IDR_IDR_14)
	{
		GPIOC->BSRR |= GPIO_BSRR_BR_14;
	}
	else
	{
		GPIOC->BSRR |= GPIO_BSRR_BS_14;
	}
}

/**
 * toggles the led
 * @param NONE
 * @retval NONE
 */
void toggleOrange(void)
{
	if(GPIOC->IDR & GPIO_IDR_IDR_13)
	{
		GPIOC->BSRR |= GPIO_BSRR_BR_13;
	}
	else
	{
		GPIOC->BSRR |= GPIO_BSRR_BS_13;
	}
}
