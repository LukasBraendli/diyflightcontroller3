/*
 * pid_controller.c
 *
 *  Created on: 13.08.2018
 *      Author: IchWerSonst
 */


/* Includes ------------------------------------------------------------------*/
#include "pid_controller.h"
#include "useful_functions.h"
#include "stdint.h"
#include "bat_measure.h"
#include "rates_and_gains.h"


/* functions ------------------------------------------------------------------*/

/**
 * @brief initializes the pid controller by creating a new structure instance,
 * 		filling it with the given information and returning it
 * @param _Kp, _Ki, _Kd the Proportional, Integral and Derivative gains
 * @param tau the D term low pass filter time constant
 * @param _rangeMin the minimal controller output
 * @param _rangeMax the maximal controller output
 * @retval PIDController_t instance
 */
void pid_init(PIDController_t *pid, float _Kp, float _Ki, float _Kd, float _tau, float _rangeMin, float _rangeMax)
{
	pid->Kp = _Kp;					// P gain
	pid->Ki = _Ki;					// I gain
	pid->Kd = _Kd;					// D gain
	pid->tau = _tau;				// D term filter constant
	pid->rangeMin = _rangeMin;		// minimal controller output
	pid->rangeMax = _rangeMax;		// maximal controller output
	pid->lastTime_us = 0;
	pid->integrator = 0.0f;
	pid->differentiator = 0.0f;
	pid->prevError = 0.0f;
	pid->prevMeasurement = 0.0f;
	pid->out = 0.0f;

	// throttle Ki adjust
	pid->lastIGainAdjust_ms = 0;
	pid->lastTrhottle = 0.0;
	pid->KiBoost = 1.0;
}


float pid_update(PIDController_t *pid, float setpoint, float measurement)
{
	// calculate sample time T
	unsigned long currentTime_us = 0;//microseconds();
	double T = 0.001;	// sample time in seconds

	// calculate error
	float error = setpoint - measurement;

	/*
	 * Proportional
	 */
	float proportional = pid->Kp * error;

	/*
	 * Integral
	 */
	pid->integrator += 0.5f * pid->Ki * pid->KiBoost * T * (error + pid->prevError);

	// Anti windup via dynamic integrator clamping
	float rangeMinInt, rangeMaxInt;

	// Calculate Integrator limits
	if(pid->rangeMax > proportional)
	{
		rangeMaxInt = pid->rangeMax - proportional;
	}
	else
	{
		rangeMaxInt = 0.0f;
	}

	if(pid->rangeMin < proportional)
	{
		rangeMinInt = pid->rangeMin - proportional;
	}
	else
	{
		rangeMinInt = 0.0f;
	}

	// Clamp integrator
	pid->integrator = constrain_float(pid->integrator, rangeMinInt, rangeMaxInt);

	/*
	 * Derivative
	 */
	//pid->differentiator = pid->Kd * (error - pid->prevError) / T;

	pid->differentiator = (2.0f * pid->Kd * (measurement - pid->prevMeasurement) * (-1)	// derivative on error --> NO  * (-1) needed!; derivative on measurement --> * (-1) needed!
						+ (2.0f * pid->tau - T) * pid->differentiator)
						/ (2.0f * pid->tau + T);


	/*
	 * Calculate control value and apply limits
	 */
	pid->out = proportional + pid->integrator + pid->differentiator;

	pid->out = constrain_float(pid->out, pid->rangeMin, pid->rangeMax);

	/*
	 * Store time and error for next update
	 */
	pid->lastTime_us = currentTime_us;

	pid->prevError = error;
	pid->prevMeasurement = measurement;

	/*
	 * Return the control output
	 */
	return pid->out;
}


void pid_setPGain(PIDController_t * pid, float _Kp)
{
	pid->Kp = _Kp;
}


void pid_setIGain(PIDController_t * pid, float _Ki)
{
	pid->Ki = _Ki;
}


void pid_setDGain(PIDController_t * pid, float _Kd)
{
	pid->Kd = _Kd;
}


float pid_getPGain(PIDController_t * pid)
{
	return pid->Kp;
}


float pid_getIGain(PIDController_t * pid)
{
	return pid->Ki;
}


float pid_getDGain(PIDController_t * pid)
{
	return pid->Kd;
}


void pid_setGain(PIDController_t *pid, float _Kp, float _Ki, float _Kd)
{
	pid->Kp = _Kp;
	pid->Ki = _Ki;
	pid->Kd = _Kd;
}


void pid_reset(PIDController_t *pid)
{
	pid->integrator = 0.0f;
	pid->differentiator = 0.0f;
	pid->prevError = 0.0f;
	pid->prevMeasurement = 0.0f;
	pid->out = 0.0f;
}


void pid_throttleIGainAdjust(PIDController_t * pid, float throttle, float throttle_min, float throttle_max)
{
	uint32_t now = HAL_GetTick();

	if(now > pid->lastIGainAdjust_ms + 100)	//check every 100 ms
	{
		float deltaThrottle = throttle - pid->lastTrhottle;
		if(deltaThrottle < 0)
		{
			deltaThrottle = (-1) * deltaThrottle;
		}
		if(deltaThrottle > (THROTTLE_KI_THRESHOLD * (throttle_max - throttle_min)))
		{
			pid->KiBoost = 3.0;
		}
		else
		{
			pid->KiBoost = 1.0;
		}
		pid->lastIGainAdjust_ms = now;
		pid->lastTrhottle = throttle;
	}
}
