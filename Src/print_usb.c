/*
 * print_usb.c
 *
 *  Created on: 09.04.2020
 *      Author: IchWerSonst
 */

#include "print_usb.h"
#include "usbd_def.h"


/**
 * @brief initializes the print over usb functionality
 */
void printUsb_init(void)
{
	usbTxBufSize = 0;
}


/**
 * @brief calculates number of digits in integer
 * @param n the integer
 * @retval the number of digits in the integer
 */
int numDigits(long n)
{
	int digits = 1;

	n = (n < 0) ? -n : n;

	while(n > 9)
	{
		n /= 10;
		digits++;
	}
	return digits;
}


/**
 * @brief converts a given integer to a c string (char array)
 * @param n the integer to convert
 * @param digits the number of digits that should be printed
 * 		--> i.e. n = 5, digits = 3, buffer large enough --> it will print 005
 * @param buf pointer to char array to store the new string
 * @param size the size of the given char array
 * @retval the length of the generated string excluding the NULL termination
 * 		or -1 if the given buffer is too short
 */
int8_t numToStrHelper(long n, uint8_t digits, char *buf, size_t size)
{
	uint8_t negative = n < 0;

	if(digits + negative + 1 > size)	//if given buffer is too small
	{
		return -1;
	}

	buf[digits + negative] = '\0';	// terminate string with NULL

	if(negative)
	{
		buf[0] = '-';
		n *= -1;
	}

	for(int i = digits + negative - 1; i >= negative; i--)
	{
		buf[i] = '0' + n % 10;
		n /= 10;
	}

	return digits + negative;	// return length of string, excluding NULL termination
}


/**
 * @brief converts a given integer to a c string (char array)
 * @param n the integer to convert
 * @param buf pointer to char array to store the new string
 * @param size the size of the given char array
 * @retval the length of the generated string excluding the NULL termination
 * 		or -1 if the given buffer is too short
 */
int8_t numToStr(long n, char *buf, size_t size)
{
	int digits = numDigits(n);

	return numToStrHelper(n, digits, buf, size);
}


/**
 * @brief converts floating point number to c string (char array)
 * @param n the number to convert
 * @param decimals the number of decimal points to convert
 * @param buf pointer to char array to store the new string
 * @param size the size of the given char array
 * @retval the length of the generated string excluding the NULL termination
 * 		or -1 if the given buffer is too short
 */
int8_t floatToStr(double n, uint16_t decimals, char *buf, size_t size)
{
	long integerPart = (long) n;

	uint8_t negative = n < 0;

	double frac = negative ? (integerPart-n) : (n-integerPart);
	long decimalPart = (long) (frac * pow(10, decimals) + 0.5);	// +0.5 for rounding

	int digits = numDigits(integerPart);

	if(digits + decimals + negative + 2 > size)	//if given buffer is too small
	{
		return -1;
	}

	if(decimals == 0)
	{
		return numToStr(integerPart, buf, size);
	}

	numToStr(integerPart, buf, size);	// convert integer part

	buf[negative + digits] = '.';	// add decimal point

	numToStrHelper(decimalPart, decimals, buf + negative + digits + 1, size - (negative + digits + 1));	// convert decimal part

	return(negative + digits + 1 + decimals);
}


/**
 * @brief print an integer in ascii format via usb vcp
 * @param n the number to print
 */
void printNum(long n)
{
	uint8_t negative = n < 0;
	uint8_t digits = numDigits(n);
	char str[negative + digits + 1];
	int8_t size = numToStr(n, str, sizeof(str));

	if(size > 0)
	{
		usbFillTxBuf(str, (uint8_t)size);
	}
}


/**
 * @brief print a float in ascii format via usb vcp
 * @param n the number to print
 * @param decimals the number of decimal points to print
 */
void printFloat(double n, uint16_t decimals)
{
	uint8_t negative = n < 0;
	uint8_t digits = numDigits((long)n);
	char str[negative + digits + 1 + decimals + 1];
	int8_t size = floatToStr(n, decimals, str, sizeof(str));

	if(size > 0)
	{
		usbFillTxBuf(str, (uint8_t)size);
	}
}


/**
 * @brief print a c string (NULL terminated char array) over usb vcp
 * @param str pointer to the character array to print
 */
void printStr(char *str)
{
	uint16_t i = 0;
	while(str[i] != '\0')
	{
		i++;
	}

	usbFillTxBuf(str, i);
}


/**
 * @brief print a newline via usb vcp
 */
void printLn(void)
{
	char buf = '\n';
	usbFillTxBuf(&buf, 1);
}


/**
 * @brief print a tabulator via usb vcp
 */
void printTab(void)
{
	char buf = '\t';
	usbFillTxBuf(&buf, 1);
}


/**
 * @brief print a carriage return via usb vcp
 */
void printCr(void)
{
	char buf = '\r';
	usbFillTxBuf(&buf, 1);
}


/**
 * @brief send content of usbTxBuf via usb
 */
USBD_StatusTypeDef usbSend(void)
{
	USBD_StatusTypeDef usbstatus = USBD_OK;
	if(usbTxBufSize != 0)
	{
		usbstatus = CDC_Transmit_FS(usbTxBuf, usbTxBufSize);
		if(usbstatus != USBD_BUSY)
		{
			usbTxBufSize = 0;
		}
	}
	return usbstatus;
}


/**
 * @brief appends the given buffer to the transmit buffer
 * @param buf pointer to the buffer to append/send
 * @param size the size of the buffer to append
 */
void usbFillTxBuf(char *buf, size_t size)
{
	if(usbTxBufSize + size <= TX_BUF_LEN && size != 0)	// if there is enough space in the tx buffer
	{
		for(uint16_t i = 0; i < size; i++)
		{
			usbTxBuf[usbTxBufSize + i] = (uint8_t)(buf[i]);
		}

		usbTxBufSize += size;
	}
}


/**
 * @brief clears the transmit buffer
 */
void usbClearTxBuf(void)
{
	usbTxBufSize = 0;
}


/**
 * @brief initialize the read functionalities for usb vcp
 */
void readUsb_init(void)
{
	usbRxBuf_s.head = 0;
	usbRxBuf_s.tail = 0;
	usbRxBuf_s.dataAvailable = false;
	usbRxBuf_s.packetLost = false;
}

