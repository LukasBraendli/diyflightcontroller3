/*
 * escTlm.c
 *
 *  Created on: Aug 31, 2020
 *      Author: lukas
 */

#include "escTlm.h"
#include "motors.h"
#include "leds.h"
#include "print_usb.h"


/**
 * initializes the UART for the ESC Telemetry connection
 */
void escTlm_init()
{
	/* clock configuration for uart 4 */
	// Select System clock as source for uart 4
	RCC->DCKCFGR2 |= RCC_DCKCFGR2_UART4SEL_0;
	RCC->DCKCFGR2 &= ~((uint32_t)RCC_DCKCFGR2_UART4SEL_1);
	__HAL_RCC_UART4_CLK_ENABLE();	// enable uart 4 clock

	/* initialize uart 4 for receive only */
	UART4->CR1 &= ~((uint32_t)USART_CR1_M);	// wordlength 8 data bits
	// oversampling = 16 --> TX/RX baud = f_ck/(BRR[15:0])
	UART4->BRR |= (uint32_t)(HAL_RCC_GetSysClockFreq()/115200);	// baudrate = 115200
	UART4->CR2 &= ~((uint32_t)USART_CR2_STOP);	// 1 stop bit
	UART4->CR1 &= ~((uint32_t)USART_CR1_PCE);	// disable parity control
	UART4->CR1 &= ~((uint32_t)USART_CR1_OVER8);	// oversampling by 16
	UART4->CR1 |= (uint32_t)USART_CR1_RXNEIE;	// enable RX not empty interrupt
	UART4->CR1 |= (uint32_t)USART_CR1_UE;	// enable uart 4
	UART4->CR1 |= (uint32_t)USART_CR1_RE;	// enable uart 4 receiver

	/* initialize variables */
	escTlm_num_bytes_received = 0;
	for(int i = 0; i < ESCTLM_PACKET_SIZE; i++)
	{
		escTlm_temp_buf[i] = (uint8_t)0;
	}
	escTlm_current_status = data_old;

	/* enable interrupt generation */
	HAL_NVIC_SetPriority(UART4_IRQn, 0, 0);
	HAL_NVIC_EnableIRQ(UART4_IRQn);
}


/**
 * interrupt routine for uart4 interrupts
 */
void escTlm_uart_read()
{
	//printStr("INT");
	//usbSend();
	// if uart receive not empty interrupt
	blinkRed(200,200);
	blinkOrange(200,200);
	printf("[INFO] Interrupt routine entered.");
	usbSend();
	if((UART4->ISR & USART_ISR_RXNE) == 1)
	{
		redOn();
		// if no errors occured
		if((UART4->ISR & (USART_ISR_ORE | USART_ISR_NE | USART_ISR_FE)) == 0)
		{
			escTlm_temp_buf[escTlm_num_bytes_received] = UART4->RDR;
			escTlm_num_bytes_received++;
			if(escTlm_num_bytes_received == ESCTLM_PACKET_SIZE)
			{
				escTlm_current_status = data_new;
				escTlm_num_bytes_received = 0;
			}
			else
			{
				escTlm_current_status = updating;
			}
		}
		else
		{
			escTlm_current_status = data_invalid;
		}
	}
}


/**
 * tries to read the received esc telemetry
 * @param buf pointer to the buffer in which to store the telemetry information
 * @retval 	0 iff the data was received without error
 * 			1 iff there was a noise or framing error
 * 			2 iff there was no new data received or if the data is being updated
 */
uint8_t escTlm_read(uint8_t *buf)
{
	if(escTlm_current_status == data_invalid)
	{
		return 1;
	}

	if(escTlm_current_status == data_old || escTlm_current_status == updating)
	{
		return 2;
	}

	// copy data from temporary buffer escTlm_temp_buf to given buffer buf
	for(int i = 0; i < ESCTLM_PACKET_SIZE; i++)
	{
		buf[i] = escTlm_temp_buf[i];
	}
	escTlm_current_status = data_old;
	return 0;
}


/**
 * getter functions for
 * - temperature [°C]
 * - voltage in [V] (Volts)
 * - electrical current [A] (Ampères)
 * - power consuption [mAh] (milliAmpHours)
 * - motor RPM [rpm] (rotations per minute)
 * from a esc telemetry package
 * @param tlm_buf pointer to the telemetry package
 */
uint8_t escTlm_get_temperature(uint8_t *tlm_buf)
{
	return tlm_buf[0];
}

float escTlm_get_voltage(uint8_t *tlm_buf)
{
	uint16_t tmp = (tlm_buf[1] << 8) | tlm_buf[2];
	return (float)tmp / 100;
}

float escTlm_get_current(uint8_t *tlm_buf)
{
	uint16_t tmp = (tlm_buf[3] << 8) | tlm_buf[4];
	return (float)tmp / 100;
}

uint16_t escTlm_get_consumption(uint8_t *tlm_buf)
{
	return (tlm_buf[5] << 8) | tlm_buf[6];
}

double escTlm_get_rpm(uint8_t *tlm_buf)
{
	double erpm = (double)((tlm_buf[7] << 8) | tlm_buf[8]) * 100.;
	return erpm/(MOTOR_POLE_COUNT / 2);
}


/**
 * calculates CRC number for given telemetry packet
 * and compares it to the given CRC number
 * @param buf pointer to telemetry packet
 * @retval 0 iff crc is wrong (bad); else 1 (good)
 */
uint8_t escTlm_crc_valid(uint8_t *buf)
{
	uint8_t given_crc = buf[ESCTLM_PACKET_SIZE - 1];
	uint8_t calculated_crc = get_crc8(buf, ESCTLM_PACKET_SIZE - 1);

	return (uint8_t)(given_crc == calculated_crc);
}


/**
 * functions provided by Flyduino KISS
 */
uint8_t update_crc8(uint8_t crc, uint8_t crc_seed)
{
	uint8_t crc_u;
	uint8_t i;
	crc_u = crc;
	crc_u ^= crc_seed;
	for (i=0; i<8; i++)
	{
		crc_u = ( crc_u & 0x80 ) ? 0x7 ^ ( crc_u << 1 ) : ( crc_u << 1 );
	}
	return (crc_u);
}

uint8_t get_crc8(uint8_t *Buf, uint8_t BufLen)
{
	uint8_t crc = 0;
	uint8_t i;
	for(i=0; i<BufLen; i++)
	{
		crc = update_crc8(Buf[i], crc);
	}
	return (crc);
}
