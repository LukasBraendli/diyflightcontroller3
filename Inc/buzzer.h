/*
 * leds.h
 *
 *  Created on: 15.02.2020
 *      Author: IchWerSonst
 */

#ifndef BUZZER_H
#define BUZZER_H_

#include "main.h"


/* Defines ---------------------------------------------*/
#define BUZZER_OFF_VALUE	1500	// pwm value: 1500us duty cycle
#define BUZZER_ON_VALUE		1200	// pwm value: 1200us duty cycle

//#define BUZZER_UPDATE_INTERVAL_MS	10 //update pwm value every x milliseconds


/* Variables ---------------------------------------------*/
uint16_t buzzer_status;	// BUZZER_ON_VALUE or BUZZER_OFF_VALUE
//uint32_t buzzer_lastUpdate;

/* Functions prototypes ---------------------------------------------*/
void buzzer_init(void);
//void beep(uint16_t on_ms, uint16_t off_ms);
void buzzerOn(void);
void buzzerOff(void);
void toggleBuzzer(void);
//void buzzer_scheduler(void);

#endif /* BUZZER_H_ */
