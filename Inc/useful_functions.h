/*
 * useful_functions.h
 *
 *  Created on: 11.08.2018
 *      Author: IchWerSonst
 */

#include "main.h"

#ifndef USEFUL_FUNCTIONS_H_
#define USEFUL_FUNCTIONS_H_

//---------- function prototypes ----------
long map(long , long , long , long , long);
double map_float(double , double , double , double , double);
long constrain(long , long , long);
double constrain_float(double , double , double);
uint8_t update(uint32_t interval_ms);

#endif /* USEFUL_FUNCTIONS_H_ */
