/*
 * motors.h
 *
 *  Created on: Aug 26, 2020
 *      Author: lukas
 */

#ifndef MOTORS_H_
#define MOTORS_H_

#define MOTOR_POLE_COUNT	14

typedef enum{
	back_right,
	back_left,
	front_left,
	front_right
}motor_pos_quad_X_t;

typedef enum{
	cw = 1,		//clockwise
	ccw = -1	//counterclockwise
}motor_direction_t;

typedef struct{
	motor_pos_quad_X_t position;
	motor_direction_t direction;
	float rpm;
	float current;
}motor_t;

#endif /* MOTORS_H_ */
