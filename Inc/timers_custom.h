/*
 * timers_custom.h
 *
 *  Created on: 18.04.2020
 *      Author: IchWerSonst
 */

#ifndef TIMERS_CUSTOM_H_
#define TIMERS_CUSTOM_H_

#include "main.h"

void TIM2_Init(void);
uint32_t microseconds();	// returns TIM2 counter register, works up to 70 minutes of processor on-time
void TIM3_Init(void);
void TIM5_Init(void);

#endif /* TIMERS_CUSTOM_H_ */
