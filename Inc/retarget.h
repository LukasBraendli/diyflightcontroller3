/*

 * retarget.h
 *
 *  Created on: Sep 6, 2020
 *      Author: lukas
 *
 * All credit to Carmine Noviello for this code
 * https://github.com/cnoviello/mastering-stm32/blob/master/nucleo-f030R8/system/include/retarget/retarget.h
 */

#ifndef RETARGET_H_
#define RETARGET_H_

#include "stm32f7xx_hal.h"
#include <sys/stat.h>

void RetargetInit();
int _isatty(int fd);
int _write(int fd, char* ptr, int len);
int _close(int fd);
int _lseek(int fd, int ptr, int dir);
int _read(int fd, char* ptr, int len);
int _fstat(int fd, struct stat* st);

#endif /* RETARGET_H_ */
