/* USER CODE BEGIN Header */
/**
  ******************************************************************************
  * @file           : main.h
  * @brief          : Header for main.c file.
  *                   This file contains the common defines of the application.
  ******************************************************************************
  * @attention
  *
  * <h2><center>&copy; Copyright (c) 2020 STMicroelectronics.
  * All rights reserved.</center></h2>
  *
  * This software component is licensed by ST under BSD 3-Clause license,
  * the "License"; You may not use this file except in compliance with the
  * License. You may obtain a copy of the License at:
  *                        opensource.org/licenses/BSD-3-Clause
  *
  ******************************************************************************
  */
/* USER CODE END Header */

/* Define to prevent recursive inclusion -------------------------------------*/
#ifndef __MAIN_H
#define __MAIN_H

#ifdef __cplusplus
extern "C" {
#endif

/* Includes ------------------------------------------------------------------*/
#include "stm32f7xx_hal.h"

/* Private includes ----------------------------------------------------------*/
/* USER CODE BEGIN Includes */
#include <stdbool.h>
/* USER CODE END Includes */

/* Exported types ------------------------------------------------------------*/
/* USER CODE BEGIN ET */

/* USER CODE END ET */

/* Exported constants --------------------------------------------------------*/
/* USER CODE BEGIN EC */

/* USER CODE END EC */

/* Exported macro ------------------------------------------------------------*/
/* USER CODE BEGIN EM */

/* USER CODE END EM */

/* Exported functions prototypes ---------------------------------------------*/
void Error_Handler(void);

/* USER CODE BEGIN EFP */

/* USER CODE END EFP */

/* Private defines -----------------------------------------------------------*/
#define LED_ORANGE_Pin GPIO_PIN_13
#define LED_ORANGE_GPIO_Port GPIOC
#define LED_RED_Pin GPIO_PIN_14
#define LED_RED_GPIO_Port GPIOC
#define ICM20602_INT_Pin GPIO_PIN_15
#define ICM20602_INT_GPIO_Port GPIOC
#define ICM20602_INT_EXTI_IRQn EXTI15_10_IRQn
#define BAT_MEASURE_Pin GPIO_PIN_0
#define BAT_MEASURE_GPIO_Port GPIOC
#define MOTOR1_Pin GPIO_PIN_0
#define MOTOR1_GPIO_Port GPIOA
#define MOTOR2_Pin GPIO_PIN_1
#define MOTOR2_GPIO_Port GPIOA
#define MOTOR3_Pin GPIO_PIN_2
#define MOTOR3_GPIO_Port GPIOA
#define MOTOR4_Pin GPIO_PIN_3
#define MOTOR4_GPIO_Port GPIOA
#define PDB_BUZZER_Pin GPIO_PIN_0
#define PDB_BUZZER_GPIO_Port GPIOB
#define Gyro_NSS_Pin GPIO_PIN_12
#define Gyro_NSS_GPIO_Port GPIOB
#define USB_SENS_Pin GPIO_PIN_14
#define USB_SENS_GPIO_Port GPIOB
#define ESC_TLM_RX_Pin GPIO_PIN_11
#define ESC_TLM_RX_GPIO_Port GPIOC
#define SBus_RX_Pin GPIO_PIN_2
#define SBus_RX_GPIO_Port GPIOD
/* USER CODE BEGIN Private defines */

/* USER CODE END Private defines */

#ifdef __cplusplus
}
#endif

#endif /* __MAIN_H */

/************************ (C) COPYRIGHT STMicroelectronics *****END OF FILE****/
