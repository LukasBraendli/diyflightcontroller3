/*
 * print_usb.h
 *
 *  Created on: 09.04.2020
 *      Author: IchWerSonst
 */

#ifndef PRINT_USB_H_
#define PRINT_USB_H_

#include "main.h"
#include <math.h>
#include "usbd_def.h"

/* defines */
#define USB_NUM_RETRIES_TX	150
#define TX_TIMEOUT_MS	1
#define TX_BUF_LEN	200	// maximal 200 characters/bytes in transmit buffer


/* variables */
char usbTxBuf[TX_BUF_LEN];
uint16_t usbTxBufSize;


/* usbRead */
#define USB_RX_PACKET_SIZE	2048	// mirrors APP_RX_DATA_SIZE in file "usbd_cdc_if.c"
// !!! do NOT make MAX_PACKETS_IN_BUFFER larger than 255 !!!
#define MAX_PACKETS_IN_RX_BUF 10 //max that many packets can be received and saved without overwriting. Each packet has a max size of APP_RX_DATA_SIZE = USB_RX_PACKET_SIZE

struct usbRxBuf_s{
	uint8_t head;	// current position in buffer to save incoming data
	uint8_t tail;	// index of data in buffer which has been processed
	// head=tail --> all data were processed, waiting for new data coming

	bool dataAvailable; //1 --> data was received. 0 --> no data available

	// could save <MaxCommandsInBuffer> number of commands of maximal size <APP_RX_DATA_SIZE>
	uint8_t usbRxBuf[MAX_PACKETS_IN_RX_BUF][USB_RX_PACKET_SIZE];

	//save the size of each packet
	uint8_t packetSizes[MAX_PACKETS_IN_RX_BUF];

	bool packetLost;	// 1 --> buffer has overflown --> oldest packet(s) has(have) been lost
} usbRxBuf_s;



/* function prototypes */
extern uint8_t CDC_Transmit_FS(uint8_t* Buf, uint16_t Len);

void printUsb_init(void);
int numDigits(long n);
int8_t numToStrHelper(long n, uint8_t digits, char *buf, size_t size);
int8_t numToStr(long n, char *buf, size_t size);
int8_t floatToStr(double n, uint16_t decimals, char *buf, size_t size);
void printNum(long n);
void printFloat(double n, uint16_t decimals);
void printStr(char *str);
void printLn(void);
void printTab(void);
void printCr(void);
USBD_StatusTypeDef usbSend(void);
void usbFillTxBuf(char *buf, size_t size);
void usbClearTxBuf(void);

void readUsb_init(void);


#endif /* PRINT_USB_H_ */
