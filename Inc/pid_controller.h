/*
 * pid_controller.h
 *
 *  Created on: 13.08.2018
 *      Author: IchWerSonst
 */

#ifndef PID_CONTROLLER_H_
#define PID_CONTROLLER_H_

#include <stdint.h>

/* typedefs ------------------------------------------------------------------*/
typedef struct
{
	// controller gains
	float Kp, Ki, Kd;

	// Derivative low pass filter time constant
	float tau;

	// Output limits
	float rangeMin, rangeMax;

	// Last update time in microseconds
	unsigned long lastTime_us;

	// Controller "memory"
	float integrator;
	float differentiator;
	float prevError;
	float prevMeasurement;

	// Output control value
	float out;

	// Throttle I Gain adjustment
	uint32_t lastIGainAdjust_ms;
	float lastTrhottle;
	float KiBoost;

	// Autotuning enabled/disabled
	uint8_t autotuning_enabled;
	float autotune_rangeMin;
	float autotune_rangeMax;

} PIDController_t;


/* function prototypes ------------------------------------------------------------------*/
void pid_init(PIDController_t *pid, float _Kp, float _Ki, float _Kd, float _tau, float _rangeMin, float _rangeMax);
float pid_update(PIDController_t *pid, float setpoint, float measurement);
void pid_setPGain(PIDController_t * pid, float _Kp);
void pid_setIGain(PIDController_t * pid, float _Ki);
void pid_setDGain(PIDController_t * pid, float _Kd);
float pid_getPGain(PIDController_t * pid);
float pid_getIGain(PIDController_t * pid);
float pid_getDGain(PIDController_t * pid);
void pid_setGain(PIDController_t *pid, float _Kp, float _Ki, float _Kd);
void pid_reset(PIDController_t *);
void pid_throttleIGainAdjust(PIDController_t * pid, float throttle, float throttle_min, float throttle_max);


#endif /* PID_CONTROLLER_H_ */
