/*
 * bat_measure.h
 *
 *  Created on: 07.06.2020
 *      Author: IchWerSonst
 */

#ifndef BAT_MEASURE_H_
#define BAT_MEASURE_H_


#include "main.h"

#define ADC_NUM_SAMPLES			0b100
#define NUM_BAT_MEAS_SAMPLES	10	// how many samples to average over
#define VOLTAGE_DIV_RATIO		13	// Multiply the measured value by 13 to get the real value


#define LIPO_1S			1
#define LIPO_1S_VMAX	4.2
#define LIPO_1S_VCRIT	3.5
#define LIPO_1S_VMIN	3.0

#define LIPO_3S			3
#define LIPO_3S_VMAX	LIPO_3S * LIPO_1S_VMAX
#define LIPO_3S_VCRIT	LIPO_3S * LIPO_1S_VCRIT
#define LIPO_3S_VMIN	LIPO_3S * LIPO_1S_VMIN

#define LIPO_4S			4
#define LIPO_4S_VMAX	LIPO_4S * LIPO_1S_VMAX
#define LIPO_4S_VCRIT	LIPO_4S * LIPO_1S_VCRIT
#define LIPO_4S_VMIN	LIPO_4S * LIPO_1S_VMIN

#define LIPO_CELL_ERR	-1

/*
 * When using higher voltage battery than 3 cell (i.e. 4S <=> 4cell)
 * the pid values need to be scaled down because the system parameters changed
 * (i.e. the motors are faster at the same throttle value)
 * the PID gains should be tuned at 3S <=> 3cell <=> 100% voltage <=> 100% PID gains
 * 		==> 4S <=> 4cell <=> ~133% voltage <=> ~75% PID gains
 */
#define PID_3S_SCALAR 1.0
#define PID_4S_SCALAR 0.75


/* variables ---------------------------------------- */
volatile uint16_t num_current_bat_meas_samples;
volatile double bat_meas_ADCAvg;
volatile float bat_meas_batVoltage;	// battery voltage in volts
volatile uint8_t bat_meas_busyFlag;

int8_t lipo_num_cells;	// number of detected cells in battery; -1 iff error
float lipo_vmax;
float lipo_vcrit;
float lipo_vmin;

float lipo_pid_scalar;

/* function prototypes ------------------------------ */
void bat_measure_init(void);
void bat_measure_start(void);
void bat_measure_EOCHandler(void);
float bat_measure_getBatVoltage(void);
float bat_measure_getPIDScalar(void);

#endif /* BAT_MEASURE_H_ */
