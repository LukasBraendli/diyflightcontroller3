/*
 * rates_and_gains.h
 *
 *  Created on: 09.06.2020
 *      Author: IchWerSonst
 */

#ifndef RATES_AND_GAINS_H_
#define RATES_AND_GAINS_H_


/*
 * PID
 */
// Maximal gain values when tuning
#define K_P_MAX	3
#define K_I_MAX	10
#define K_D_MAX	1

#define K_P_YAW_MAX	3
#define K_I_YAW_MAX	10
#define K_D_YAW_MAX	1

// Default gain values for 3 cell LiPo (nominal voltage = 3*3.7V = 11.1V)
#define KP_PITCH_3S		1.7f//1.9f	/* oscillations @ 2.4f w 100Hz LPF */
#define KI_PITCH_3S		2.3//2.0f
#define KD_PITCH_3S		0.04f//0.035f

#define KP_ROLL_3S		1.45f//1.9f	/* oscillations @ 2.2f w 100Hz LPF */
#define KI_ROLL_3S		2.3//2.0f
#define KD_ROLL_3S		0.042f//0.035f

#define KP_YAW_3S	1.8//2.0
#define KI_YAW_3S	2.0//1.5
#define KD_YAW_3S	0.0

// D-term low-pass filter constant
#define TAU_PITCH	0.0125
#define TAU_ROLL		0.0125
#define TAU_YAW		0.0125

// maximal controller output
#define PID_RANGE_MAX	500.0
#define PID_RANGE_MIN	(-1) * PID_RANGE_MAX

//throttle I gain adjust threshold in percent of maximal throttle range
#define THROTTLE_KI_THRESHOLD	35.0

/*
 * Rates
 */
#define RC_RATE_MAX	600				// degrees per second at maximal stick deflection
#define RC_RATE_MIN (-1) * RC_RATE_MAX	// degrees per second at minimal stick deflection

#define RC_YAW_RATE_MAX	400
#define RC_YAW_RATE_MIN	(-1) * RC_YAW_RATE_MAX


#endif /* RATES_AND_GAINS_H_ */
