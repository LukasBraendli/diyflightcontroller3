/*
 * icm20602.h
 *
 *  Created on: 08.07.2018
 *      Author: IchWerSonst
 */

#ifndef ICM20602_H_
#define ICM20602_H_


/* Includes ------------------------------------------------------------------*/
#include "stm32f7xx_hal.h"
#include "spi.h"


/* Variables -----------------------------------------------------------------*/
volatile uint8_t icm20602_intFlag;


/* Constants -----------------------------------------------------------------*/
// REGISTERS
#define ICM20602_SELF_TEST_X_ACCEL		0x0D
#define ICM20602_SELF_TEST_Y_ACCEL		0x0E
#define ICM20602_SELF_TEST_Z_ACCEL		0x0F
#define ICM20602_SELF_TEST_X_GYRO		0x50
#define ICM20602_SELF_TEST_Y_GYRO		0x51
#define ICM20602_SELF_TEST_Z_GYRO		0x52
#define ICM20602_XG_OFFS_USRH			0x13
#define ICM20602_XG_OFFS_USRL			0x14
#define ICM20602_YG_OFFS_USRH			0x15
#define ICM20602_YG_OFFS_USRL			0x16
#define ICM20602_ZG_OFFS_USRH			0x17
#define ICM20602_ZG_OFFS_USRL			0x18
#define ICM20602_XA_OFFSET_H			0x77
#define ICM20602_XA_OFFSET_L			0x78
#define ICM20602_YA_OFFSET_H			0x7A
#define ICM20602_YA_OFFSET_L			0x7B
#define ICM20602_ZA_OFFSET_H			0x7D
#define ICM20602_ZA_OFFSET_L			0x7E
#define ICM20602_SMPLRT_DIV				0x19
#define ICM20602_CONFIG					0x1A
#define ICM20602_GYRO_CONFIG			0x1B
#define ICM20602_ACCEL_CONFIG			0x1C
#define ICM20602_ACCEL_CONFIG2			0x1D
#define ICM20602_FIFO_EN				0x23
#define ICM20602_INT_PIN_CFG			0x37
#define ICM20602_INT_ENABLE				0x38
#define ICM20602_INT_STATUS				0x3A
#define ICM20602_ACCEL_XOUT_H			0x3B
#define ICM20602_ACCEL_XOUT_L			0x3C
#define ICM20602_ACCEL_YOUT_H			0x3D
#define ICM20602_ACCEL_YOUT_L			0x3E
#define ICM20602_ACCEL_ZOUT_H			0x3F
#define ICM20602_ACCEL_ZOUT_L			0x40
#define ICM20602_TEMP_OUT_H				0x41
#define ICM20602_TEMP_OUT_L				0x42
#define ICM20602_GYRO_XOUT_H			0x43
#define ICM20602_GYRO_XOUT_L			0x44
#define ICM20602_GYRO_YOUT_H			0x45
#define ICM20602_GYRO_YOUT_L			0x46
#define ICM20602_GYRO_ZOUT_H			0x47
#define ICM20602_GYRO_ZOUT_L			0x48
#define ICM20602_ACCEL_INTEL_CTRL		0x69
#define ICM20602_USER_CTRL				0x6A
#define ICM20602_PWR_MGMT_1				0x6B
#define ICM20602_PWR_MGMT_2				0x6C
#define ICM20602_I2C_IF					0x70
#define ICM20602_FIFO_WM_TH1			0x60
#define ICM20602_FIFO_WM_TH2			0x61
#define ICM20602_FIFO_COUNTH			0x72
#define ICM20602_FIFO_COUNTL			0x73
#define ICM20602_FIFO_R_W				0x74
#define ICM20602_WHO_AM_I				0x75
// CONTROL
#define ICM20602_SPI_Read_Bit			0x01
#define ICM20602_SPI_Write_Bit			0x00

#define ICM20602_GYRO_FS_250			0x00
#define ICM20602_GYRO_FS_500			0x01
#define ICM20602_GYRO_FS_1000			0x02
#define ICM20602_GYRO_FS_2000			0x03

#define ICM20602_ACCEL_FS_2				0x00
#define ICM20602_ACCEL_FS_4				0x01
#define ICM20602_ACCEL_FS_8				0x02
#define ICM20602_ACCEL_FS_16			0x03

#define ICM20602_GYRO_FIFO_ENABLE		0x01
#define ICM20602_GYRO_FIFO_DISABLE		0X00

#define ICM20602_ACCEL_FIFO_ENABLE		0x01
#define ICM20602_ACCEL_FIFO_DISABLE		0X00


uint8_t icm20602_gyroFS;


/* Function prototypes -------------------------------------------------------*/
void icm20602_EXTIHandler(uint16_t GPIO_Pin);
void icm20602_clearIntFlag();
uint8_t icm20602_whoAmI(usr_SPI_t *);
uint8_t icm20602_testConnection(usr_SPI_t *);
void icm20602_reset(usr_SPI_t *);
void icm20602_init(usr_SPI_t *);
uint8_t icm20602_getGyroFS(usr_SPI_t *);
void icm20602_setGyroFS(usr_SPI_t *, uint8_t);
uint8_t icm20602_getAccelFS(usr_SPI_t *);
void icm20602_setAccelFS(usr_SPI_t *, uint8_t);
int16_t icm20602_getXGyroOffset(usr_SPI_t *);
int16_t icm20602_getYGyroOffset(usr_SPI_t *);
int16_t icm20602_getZGyroOffset(usr_SPI_t *);
void icm20602_setGyroOffset(usr_SPI_t *, int16_t, int16_t, int16_t);
int16_t icm20602_getXAccelOffset(usr_SPI_t *);
int16_t icm20602_getYAccelOffset(usr_SPI_t *);
int16_t icm20602_getZAccelOffset(usr_SPI_t *);
void icm20602_setAccelOffset(usr_SPI_t *, int16_t, int16_t, int16_t);
uint8_t icm20602_getSmplrtDiv(usr_SPI_t *);
void icm20602_setSmplrtDiv(usr_SPI_t *, uint8_t);
uint8_t icm20602_getGyroAccelFifoEN(usr_SPI_t *);
void icm20602_setGyroFifoEN(usr_SPI_t *, uint8_t);
void icm20602_setAccelFifoEN(usr_SPI_t *, uint8_t);
uint8_t icm20602_getIntConfig(usr_SPI_t *);
void icm20602_setIntConfig(usr_SPI_t *, uint8_t);
uint8_t icm20602_getIntEnable(usr_SPI_t *);
void icm20602_setIntEnable(usr_SPI_t *, uint8_t);
uint8_t icm20602_getIntStatus(usr_SPI_t *);
int16_t icm20602_getXAccel(usr_SPI_t *);
int16_t icm20602_getYAccel(usr_SPI_t *);
int16_t icm20602_getZAccel(usr_SPI_t *);
float icm20602_getTemperature(usr_SPI_t *);
int16_t icm20602_getXGyro(usr_SPI_t *);
int16_t icm20602_getYGyro(usr_SPI_t *);
int16_t icm20602_getZGyro(usr_SPI_t *);
uint16_t icm20602_getFifoWatermark(usr_SPI_t *usr_SPI);
void icm20602_setFifoWatermark(usr_SPI_t *usr_SPI, uint16_t wm_level);
uint8_t icm20602_getFifoEN(usr_SPI_t *);
void icm20602_setFifoEN(usr_SPI_t *, uint8_t);
void icm20602_getFifo(usr_SPI_t *, uint8_t *, uint16_t);
void icm20602_getFifoFused(usr_SPI_t *usr_SPI, int16_t *data, uint16_t dataLen);
void icm20602_getFifoMotion6(usr_SPI_t *, int16_t *, uint16_t);
double icm20602_raw2DegPerSec(usr_SPI_t *usr_SPI, int16_t raw);


#endif /* ICM20602_H_ */
