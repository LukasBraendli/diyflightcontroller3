/*
 * filters.h
 *
 *  Created on: Aug 13, 2020
 *      Author: lukas
 */

#ifndef FILTERS_H_
#define FILTERS_H_

#include "main.h"

/* ----- FILTER COEFFICIENTS ----- */
/**
 * IIR Lowpass; Fs = 1000Hz; Fc = 100Hz
 */
static const double b_iir_o10_Fs1000_Fc100[5][3] = {
		{0.0875,0.1749,0.0875},
		{0.0754,0.1508,0.0754},
	    {0.0675,0.1349,0.0675},
	    {0.0627,0.1253,0.0627},
	    {0.0604,0.1208,0.0604}};

static const double a_iir_o10_Fs1000_Fc100[5][3] = {
		{1.0000,-1.4818,0.8316},
		{1.0000,-1.2772,0.5787},
		{1.0000,-1.1430,0.4128},
		{1.0000,-1.0619,0.3126},
		{1.0000,-1.0237,0.2654}};

/**
 * IIR Lowpass; Fs = 1000Hz; Fc = 80Hz
 */
static const double b_iir_o10_Fs1000_Fc80[5][3] = {
		{0.0575,0.1150,0.0575},
		{0.0507,0.1015,0.0507},
		{0.0461,0.0923,0.0461},
		{0.0433,0.0865,0.0433},
		{0.0419,0.0838,0.0419}};

static const double a_iir_o10_Fs1000_Fc80[5][3] = {
		{1.0000,-1.6298,0.8598},
		{1.0000,-1.4381,0.6411},
		{1.0000,-1.3073,0.4918},
		{1.0000,-1.2263,0.3993},
		{1.0000,-1.1876,0.3552}};

/**
 * IIR Lowpass; Fs = 1000Hz; Fc = 80Hz
 */
static const double b_iir_o10_Fs1000_Fc60[5][3] = {
	    {0.0332,0.0664,0.0332},
	    {0.0301,0.0602,0.0301},
	    {0.0279,0.0557,0.0279},
	    {0.0264,0.0529,0.0264},
	    {0.0257,0.0515,0.0257}};

static const double a_iir_o10_Fs1000_Fc60[5][3] = {
	    {1.0000,-1.7583,0.8911},
	    {1.0000,-1.5933,0.7136},
	    {1.0000,-1.4755,0.5869},
	    {1.0000,-1.4003,0.5060},
	    {1.0000,-1.3637,0.4667}};


typedef struct biquad{
	double b0,b1,b2;
	double a1,a2;
	double d1,d2;
	struct biquad *next;	// serves also as linked list
}biquad_t;


typedef enum{
	iir_lpf_o10_Fs1000_Fc100,	// infinite impulse response, low pass, order 10, sampling frequency 1000Hz, cutoff frequency 100Hz
	iir_lpf_o10_Fs1000_Fc80,	// infinite impulse response, low pass, order 10, sampling frequency 1000Hz, cutoff frequency 80Hz
	iir_lpf_o10_Fs1000_Fc60	// infinite impulse response, low pass, order 10, sampling frequency 1000Hz, cutoff frequency 60Hz
}filterType_t;


typedef struct{
	uint8_t order;
	biquad_t *bq_list;
}filter_t;


/* ----- Function prototypes ----- */
biquad_t *biquad_init(const double b[3], const double a[3]);
double biquad_evaluate(biquad_t * bq, double in);
filter_t *filter_init(filterType_t order);
double iir_evaluate(filter_t *filter, double in);


#endif /* FILTERS_H_ */
