/*
 * ringBuf.h
 * implements the functionalities of a circular Buffer or ring Buffer
 *
 *  Created on: Sep 25, 2020
 *      Author: lukas
 */

#ifndef RINGBUF_H_
#define RINGBUF_H_

#include "main.h"


/// Opaque circular buffer structure
typedef struct ringBuf_t{
	uint8_t * buffer;
	size_t head;
	size_t tail;
	size_t max; //of the buffer
	bool full;
}ringBuf_t;

/// Handle type, the way users interact with the API
typedef ringBuf_t* ringbuf_handle_t;

/// Pass in a storage buffer and size, returns a circular buffer handle
/// Requires: buffer is not NULL, size > 0
/// Ensures: ringbuf has been created and is returned in an empty state
ringbuf_handle_t ringBuf_init(uint8_t* buffer, size_t size);

/// Free a circular buffer structure
/// Requires: ringbuf is valid and created by ringBuf_init
/// Does not free data buffer; owner is responsible for that
void ringBuf_free(ringbuf_handle_t ringbuf);

/// Reset the circular buffer to empty, head == tail. Data not cleared
/// Requires: ringbuf is valid and created by ringBuf_init
void ringBuf_reset(ringbuf_handle_t ringbuf);

/// Put version 1 continues to add data if the buffer is full
/// Old data is overwritten
/// Requires: ringbuf is valid and created by ringBuf_init
void ringBuf_put(ringbuf_handle_t ringbuf, uint8_t data);

/// Put Version 2 rejects new data if the buffer is full
/// Requires: ringbuf is valid and created by ringBuf_init
/// Returns 0 on success, -1 if buffer is full
int ringBuf_put2(ringbuf_handle_t ringbuf, uint8_t data);

/// Retrieve a value from the buffer
/// Requires: ringbuf is valid and created by ringBuf_init
/// Returns 0 on success, -1 if the buffer is empty
int ringBuf_get(ringbuf_handle_t ringbuf, uint8_t * data);

/// CHecks if the buffer is empty
/// Requires: ringbuf is valid and created by ringBuf_init
/// Returns true if the buffer is empty
bool ringBuf_empty(ringbuf_handle_t ringbuf);

/// Checks if the buffer is full
/// Requires: ringbuf is valid and created by ringBuf_init
/// Returns true if the buffer is full
bool ringBuf_full(ringbuf_handle_t ringbuf);

/// Check the capacity of the buffer
/// Requires: ringbuf is valid and created by ringBuf_init
/// Returns the maximum capacity of the buffer
size_t ringBuf_getCapacity(ringbuf_handle_t ringbuf);

/// Check the number of elements stored in the buffer
/// Requires: ringbuf is valid and created by ringBuf_init
/// Returns the current number of elements in the buffer
size_t ringBuf_getSize(ringbuf_handle_t ringbuf);

//TODO: int ringBuf_get_range(ringBuf_t ringbuf, uint8_t *data, size_t len);
//TODO: int ringBuf_put_range(ringBuf_t ringbuf, uint8_t * data, size_t len);



#endif /* RINGBUF_H_ */
