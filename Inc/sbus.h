/*
 * sbus.h
 *
 *  Created on: 20.08.2018
 *      Author: IchWerSonst
 */

#ifndef SBUS_H_
#define SBUS_H_


/* Includes ------------------------------------------------------------------*/
#include "stm32f7xx_hal.h"
#include "stdint.h"
#include "string.h"


/* Variables and Defines ------------------------------------------------------------------*/
//#define SBUS_BAUDRATE	100000	// bits per second // not needed because hardcoded in uart initialization

#define SBUS_RX_SIZE	25		// sbus number of bytes in one command
#define SBUS_CHANNEL_NUMBER		18
#define SBUS_FAILSAFE_INACTIVE	0
#define SBUS_FAILSAFE_ACTIVE	1
#define SBUS_CHANNEL_OFFSET		1	// first byte in SBUS protocol is start byte, this means the index of the first channel data is 1
#define SBUS_FLAGBYTE_INDEX		23

/* SBUS DOCUMENTATION
	[Startbyte] [Data1] [Data2] .... [Data22][Flags][Endbyte]

		- Startbyte    = 11110000b (0xF0)


		- Data 1-22    = [ch1, 11bit][ch2, 11bit] .... [ch16, 11bit] (ch# = 0 bis 2047)

					   Kanal 1 benutzt 8 Bits von Data1 und 3 Bits von Data2

					   Kanal 2 benutzt restliche 5 Bits von Data2 und 6 Bits von Data3 usw.


		- Flags        Bit7 = ch17 = Schaltkanal (0x80)

					   Bit6 = ch18 = Schaltkanal (0x40)

					   Bit5 = Frame lost, entspricht roter LED am Empf�nger (0x20)

					   Bit4 = Failsafe aktiviert (0x10)

					   Bit3 = unbekannt

					   Bit2 = unbekannt

					   Bit1 = unbekannt

					   Bit0 = unbekannt


		- Endbyte      = 00000000b

 * SBUS DOCUMENTATION END */

#define SBUS_CHANNEL_MAX	1811
#define SBUS_CHANNEL_MID	992
#define SBUS_CHANNEL_MIN	172

#define SBUS_MOMENTARY_SWITCH_CHANNELINDEX	8
#define SBUS_ARMING_SWITCH_CHANNELINDEX		4
#define SBUS_THROTTLE_CHANNELINDEX			0
#define SBUS_YAW_CHANNELINDEX				3
#define SBUS_PITCH_CHANNELINDEX				2
#define SBUS_ROLL_CHANNELINDEX				1

#define SBUS_FAILSAVE_TIMEOUT_MS	300	// failsave is activated after 300ms no signal


uint8_t volatile sbus_RXbuf[SBUS_RX_SIZE];

uint8_t volatile sbus_updateFlag;

typedef struct __SBUS_t
{
	UART_HandleTypeDef *huart;
	uint16_t channels[SBUS_CHANNEL_NUMBER];
	uint8_t framelost_flag;
	uint8_t rx_failsave_flag;
	uint8_t fc_failsave_flag;
	uint32_t failsave_timer_ms;
} SBUS_t;


/* Function prototypes ------------------------------------------------------------------*/
SBUS_t sbus_begin(UART_HandleTypeDef *huart);
void sbus_uartRxCplt(UART_HandleTypeDef *huart, SBUS_t *sbus);
void sbus_clearUpdateFlag(void);
int8_t sbus_getChannels(SBUS_t *sbus);
uint8_t sbus_isFailsaveActive();

#endif /* SBUS_H_ */
