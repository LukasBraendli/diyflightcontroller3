/*
 * dshot.h
 *
 *  Created on: 31.07.2018
 *      Author: IchWerSonst
 */

#ifndef DSHOT_H_
#define DSHOT_H_

/* Includes ------------------------------------------------------------------*/
#include "stm32f7xx_hal.h"


/* Defines ------------------------------------------------------------------*/
// the minimum throttle is depending on the esc-motor combo
// if it is too low, the motor may not run smoothly around minimum throttle
// the ABSOLUTE MINIMUM IS 48! This is given by the dshot protocol itself.
#define DSHOT_MOTOR_MIN	200		// >= 48 !!
#define DSHOT_MOTOR_MAX	2047	// do NOT change

#define DSHOT_300_FREQ	300000U		// 300kHz
#define DSHOT_600_FREQ	600000U		// 600kHz
#define DSHOT_1200_FREQ	1200000U	// 1200kHz
#define DSHOT_2400_FREQ	2400000U	// 2400kHz

#define DSHOT_NUM_MOTORS	4
#define TIM5_CCR1_ADDRESS_OFFSET	0x34

#define DSHOT_CMD_SIZE (16+1)	// 11 throttle bits, 1 telemetry request bit, 4 checksum bits, 1 bit to reset pwm duty cycle to 0%; all in this order

#define DSHOT_PWM_RESOLUTION	45U
#define DSHOT_PWM_HIGH			34U
#define DSHOT_PWM_LOW			17U
#define DSHOT_PWM_FREQUENCY		DSHOT_2400_FREQ

uint32_t dshot_cmd_buf[DSHOT_CMD_SIZE * DSHOT_NUM_MOTORS];

uint8_t dshot_busy_flag;	// 0 = NOT busy; 1 = busy

/* Function prototypes ------------------------------------------------------------------*/
void dshot_init(void);
uint8_t dshot_getChecksum(uint16_t);
int8_t dshot_generateCmd(uint16_t *motor_values, uint8_t *_telemetry_request, uint16_t len);
void dshot_sendAll(void);
void dshot_setBusyFlag(void);
void dshot_clearBusyFlag(void);
uint8_t dshot_isBusy(void);
void dshot_dmaTransferCpltHandler(void);


#endif /* DSHOT_H_ */
