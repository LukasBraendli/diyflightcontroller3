/*
 * leds.h
 *
 *  Created on: 15.02.2020
 *      Author: IchWerSonst
 */

#ifndef LEDS_NOHAL_H_
#define LEDS_NOHAL_H_

#include "main.h"

/* Functions prototypes ---------------------------------------------*/
void blinkRed(uint16_t on_ms, uint16_t off_ms);
void blinkOrange(uint16_t on_ms, uint16_t off_ms);
void redOn(void);
void redOff(void);
void orangeOn(void);
void orangeOff(void);
void toggleRed(void);
void toggleOrange(void);

#endif /* LEDS_NOHAL_H_ */
