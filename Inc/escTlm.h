/*
 * escTlm.h
 *
 *  Created on: Aug 31, 2020
 *      Author: lukas
 */

#ifndef ESCTLM_H_
#define ESCTLM_H_

#include "main.h"

#define ESCTLM_PACKET_SIZE	10	// size of the telemetry packet

volatile uint8_t escTlm_num_bytes_received;
volatile uint8_t escTlm_temp_buf[ESCTLM_PACKET_SIZE];	// temporary storage for telemetry data
/* status indication:
 * 0: current data has already been read
 * 1: current data has not been read/is new
 */
enum _escTlm_status{
	data_old,	// data has already been read
	data_new,	// data has not yet been read
	updating,	// data is currently being updated
	data_invalid,	// error has occured --> invalid data
}escTlm_status;
volatile uint8_t escTlm_current_status;


/* function prototypes */
void escTlm_init();
void escTlm_uart_read();
uint8_t escTlm_read(uint8_t *buf);
uint8_t escTlm_get_temperature(uint8_t *tlm_buf);
float escTlm_get_voltage(uint8_t *tlm_buf);
float escTlm_get_current(uint8_t *tlm_buf);
uint16_t escTlm_get_consumption(uint8_t *tlm_buf);
double escTlm_get_rpm(uint8_t *tlm_buf);
uint8_t escTlm_crc_valid(uint8_t *buf);
uint8_t update_crc8(uint8_t crc, uint8_t crc_seed);
uint8_t get_crc8(uint8_t *Buf, uint8_t BufLen);

#endif /* ESCTLM_H_ */
