/*
 * interrupts.h
 *
 *  Created on: 12.04.2020
 *      Author: IchWerSonst
 */

#ifndef INTERRUPT_CALLBACKS_H_
#define INTERRUPT_CALLBACKS_H_

#include <main.h>
#include "sbus.h"

extern SBUS_t receiver;

void HAL_GPIO_EXTI_Callback(uint16_t GPIO_Pin);
void HAL_UART_RxCpltCallback(UART_HandleTypeDef *huart);

#endif /* INTERRUPT_CALLBACKS_H_ */
